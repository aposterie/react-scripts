// MIT License
// https://github.com/rejas/gulp-json-editor
//
// Copyright (c) 2019 rejas
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
const { obj } = require("through2")
const PluginError = require("plugin-error")
const { isFunction, isObjectLike } = require("lodash")

module.exports = function (edit, spaces = 2) {
  /*
   * create 'editBy' function from 'editor'
   */
  let editBy
  if (isFunction(edit)) {
    // edit JSON object by user specific function
    editBy = json => edit(json) ?? json
  } else if (isObjectLike(edit)) {
    // edit JSON object by merging with user specific object
    editBy = json => ({ ...json, ...edit })
  } else if (edit === undefined) {
    throw new PluginError("gulp-json-editor", 'missing "edit" option')
  } else {
    throw new PluginError(
      "gulp-json-editor",
      '"edit" option must be a function or an object'
    )
  }

  /*
   * create through object and return it
   */
  return obj(function (file, encoding, callback) {
    // ignore it
    if (file.isNull()) {
      this.push(file)
      return callback()
    }

    // stream is not supported
    if (file.isStream()) {
      this.emit(
        "error",
        new PluginError("gulp-json-editor", "Streaming is not supported")
      )
      return callback()
    }

    try {
      // edit JSON object and get it as string notation
      const json = JSON.stringify(
        editBy(JSON.parse(file.contents.toString("utf8"))),
        undefined,
        spaces
      )

      // write it to file
      file.contents = Buffer.from(json)
    } catch (err) {
      this.emit("error", new PluginError("gulp-json-editor", err))
    }

    this.push(file)
    callback()
  })
}

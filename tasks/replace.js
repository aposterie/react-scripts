#!/usr/bin/env node
const fs = require('fs')
const escape = require('regexp.escape')

const args = process.argv.slice(2)
if (args.length < 3) {
  throw Error('Minimum 3 arguments.')
}

const [search, replace, ...files] = args
// console.log({ search, replace, files })

const regex = RegExp(escape(search), 'g')

files.forEach(file => {
  const source = fs.readFileSync(file, 'utf8')
  fs.writeFileSync(file, source.replace(regex, replace))
})

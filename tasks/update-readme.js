#!/usr/bin/env node
const { execSync } = require("child_process")
const fs = require("fs")
const { resolve } = require("path")

const commit = execSync("git rev-parse --short HEAD", {
  cwd: resolve(__dirname, "../lib"),
})
  .toString()
  .trim()

const README = resolve(__dirname, "../README.md")

const contents = fs
  .readFileSync(README, "utf-8")
  .replace(/(yarn add gitlab:aposterie\/react-scripts)#[\w\d]+/, `$1#${commit}`)

fs.writeFileSync(README, contents)

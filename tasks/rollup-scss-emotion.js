/**
 * @returns {import("rollup").Plugin}
 */
export default () => ({
  transform(code, id) {
    if (!id.endsWith(".scss")) {
      return null
    }

    const res = code
      .split("\n")
      .map(line => line.trimRight())
      .map(line =>
        /\.\w+ \{/.test(line)
          ? `export const ${line.slice(1, -1).trim()} = css\``
          : line === "}"
          ? "`"
          : line.trimLeft().startsWith("//")
          ? ""
          : line
      )
      .filter(Boolean)
      .join("\n")

    return `import { css } from "@emotion/css";${res}`
  },
})

export function requireTSNode() {
  try {
    // Maximum compatibility because this process should
    // just be transparent to the end user and get out of the way.
    require("ts-node").register({
      transpileOnly: true,
      compilerOptions: {
        esModuleInterop: true,
        module: "CommonJS",
        target: "ES2019",
        moduleResolution: "Node",
      },
    })
  } catch {}
}

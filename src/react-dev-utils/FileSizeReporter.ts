/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import * as fs from "fs"
import * as path from "path"
import { yellow, dim, cyan, red, green } from "./chalk"
import filesize from "filesize"
import recursive from "recursive-readdir"
import stripAnsi from "strip-ansi"
import { sync as gzipSize } from "gzip-size"
import type { Stats } from "webpack"

const { log } = console

function canReadAsset(asset: string) {
  return (
    /\.(js|css)$/.test(asset) &&
    !/service-worker\.js/.test(asset) &&
    !/precache-manifest\.[0-9a-f]+\.js/.test(asset)
  )
}

// Prints a detailed summary of build files.
export function printFileSizesAfterBuild(
  webpackStats: Stats,
  previousSizeMap: FileSizes,
  buildFolder: string,
  maxBundleGzipSize: number,
  maxChunkGzipSize: number
) {
  const root = previousSizeMap.root
  const sizes = previousSizeMap.sizes
  const assets = (((webpackStats as any).stats || [webpackStats]) as Stats[])
    .map(stats =>
      stats
        .toJson({ all: false, assets: true })
        .assets!.filter(asset => canReadAsset(asset.name))
        .map(asset => {
          const fileContents = fs.readFileSync(path.join(root, asset.name))
          const size = gzipSize(fileContents)
          const previousSize = sizes[removeFileNameHash(root, asset.name)]
          const difference = getDifferenceLabel(size, previousSize)
          return {
            folder: path.join(path.basename(buildFolder), path.dirname(asset.name)),
            name: path.basename(asset.name),
            size,
            sizeLabel: filesize(size) + (difference ? " (" + difference + ")" : ""),
          }
        })
    )
    .reduce((single, all) => all.concat(single), [])

  assets.sort((a, b) => b.size - a.size)

  const longestSizeLabelLength = Math.max(
    ...assets.map(a => stripAnsi(a.sizeLabel).length)
  )

  let suggestBundleSplitting = false
  assets.forEach(asset => {
    let sizeLabel = asset.sizeLabel
    const sizeLength = stripAnsi(sizeLabel).length
    if (sizeLength < longestSizeLabelLength) {
      const rightPadding = " ".repeat(longestSizeLabelLength - sizeLength)
      sizeLabel += rightPadding
    }
    const isMainBundle = asset.name.indexOf("main.") === 0
    const maxRecommendedSize = isMainBundle ? maxBundleGzipSize : maxChunkGzipSize
    const isLarge = maxRecommendedSize && asset.size > maxRecommendedSize
    if (isLarge && path.extname(asset.name) === ".js") {
      suggestBundleSplitting = true
    }
    log(
      "  " +
        (isLarge ? yellow(sizeLabel) : sizeLabel) +
        "  " +
        dim(asset.folder + path.sep) +
        cyan(asset.name)
    )
  })
  if (suggestBundleSplitting) {
    log()
    log(yellow("The bundle size is significantly larger than recommended."))
    log(yellow("Consider reducing it with code splitting: https://goo.gl/9VhYWB"))
    log(yellow("You can also analyze the project dependencies: https://goo.gl/LeUzfb"))
  }
}

function removeFileNameHash(buildFolder: string, fileName: string) {
  return fileName
    .replace(buildFolder, "")
    .replace(/\\/g, "/")
    .replace(
      /\/?(.*)(\.[0-9a-f]+)(\.chunk)?(\.js|\.css)/,
      (_match, p1, _p2, _p3, p4) => p1 + p4
    )
}

// Input: 1024, 2048
// Output: "(+1 KB)"
function getDifferenceLabel(currentSize: number, previousSize: number) {
  const FIFTY_KILOBYTES = 1024 * 50
  const difference = currentSize - previousSize
  const fileSize = !Number.isNaN(difference) ? filesize(difference) : 0
  if (difference >= FIFTY_KILOBYTES) {
    return red("+" + fileSize)
  } else if (difference < FIFTY_KILOBYTES && difference > 0) {
    return yellow("+" + fileSize)
  } else if (difference < 0) {
    return green(fileSize)
  } else {
    return ""
  }
}

export interface FileSizes {
  root: string
  sizes: Record<string, number>
}

export function measureFileSizesBeforeBuild(buildFolder: string) {
  return new Promise<FileSizes>(resolve => {
    recursive(buildFolder, (err, fileNames) => {
      let sizes
      if (!err && fileNames) {
        sizes = fileNames.filter(canReadAsset).reduce((memo, fileName) => {
          const contents = fs.readFileSync(fileName)
          const key = removeFileNameHash(buildFolder, fileName)
          memo[key] = gzipSize(contents)
          return memo
        }, {} as Record<string, number>)
      }
      resolve({
        root: buildFolder,
        sizes: sizes || {},
      })
    })
  })
}

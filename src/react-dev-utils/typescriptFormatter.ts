/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import { EOL } from "os"
import { codeFrameColumns as codeFrame } from "@babel/code-frame"
import colors from "./chalk"
import * as fs from "fs"
import { Issue } from "fork-ts-checker-webpack-plugin/lib/issue"

const issueOrigins: Record<string, string> = {
  typescript: "TypeScript",
  internal: "fork-ts-checker-webpack-plugin",
}

function formatter(issue: Issue) {
  const { origin, severity, file, line, message, code, character } = issue

  const messageColor = severity === "warning" ? colors.yellow : colors.red
  const fileAndNumberColor = colors.bold.cyan

  const source = file && fs.existsSync(file) && fs.readFileSync(file, "utf-8")
  const frame = source
    ? codeFrame(source, { start: { line, column: character } })
        .split("\n")
        .map(str => "  " + str)
        .join(EOL)
    : ""

  return [
    messageColor.bold(`${issueOrigins[origin]} ${severity.toLowerCase()} in `) +
      fileAndNumberColor(`${file}(${line},${character})`) +
      messageColor(":"),
    message + "  " + messageColor.underline(`TS${code}`),
    "",
    frame,
  ].join(EOL)
}

export default formatter

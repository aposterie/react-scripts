/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import type { Plugin, Compiler } from "webpack"

// This webpack plugin ensures `npm install <library>` forces a project rebuild.
// We’re not sure why this isn't webpack’s default behavior.
// See https://github.com/facebook/create-react-app/issues/186.

export default class WatchMissingNodeModulesPlugin implements Plugin {
  constructor(private nodeModulesPath: string) {}

  apply(compiler: Compiler) {
    compiler.hooks.emit.tap("WatchMissingNodeModulesPlugin", compilation => {
      const missingDeps = Array.from(compilation.missingDependencies)
      const nodeModulesPath = this.nodeModulesPath

      // If any missing files are expected to appear in node_modules...
      if (missingDeps.some(file => file.includes(nodeModulesPath))) {
        // ...tell webpack to watch node_modules recursively until they appear.
        compilation.contextDependencies.add(nodeModulesPath)
      }
    })
  }
}

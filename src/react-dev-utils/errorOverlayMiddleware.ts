/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import launchEditor from "./launchEditor"
import launchEditorEndpoint from "./launchEditorEndpoint"
import type { RequestHandler } from "express-serve-static-core"

export default function createLaunchEditorMiddleware(): RequestHandler {
  return function launchEditorMiddleware(req, res, next) {
    if (req.url.startsWith(launchEditorEndpoint)) {
      const lineNumber = parseInt(req.query.lineNumber as string, 10) || 1
      const colNumber = parseInt(req.query.colNumber as string, 10) || 1
      launchEditor(req.query.fileName as string, lineNumber, colNumber)
      res.end()
    } else {
      next()
    }
  }
}

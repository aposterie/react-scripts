import { Instance } from "chalk"

const chalk = "NO_COLOR" in process.env ? new Instance({ level: 0 }) : new Instance()
export default chalk

export const {
  blue,
  bold,
  cyan,
  dim,
  green,
  grey,
  inverse,
  red,
  underline,
  yellow,
} = chalk

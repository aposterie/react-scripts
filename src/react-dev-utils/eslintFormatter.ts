/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import * as path from "path"
import { underline, red, bold, yellow } from "./chalk"
import stripAnsi from "strip-ansi"
import table from "text-table"
import type { CLIEngine, Linter } from "eslint"

const cwd = process.cwd()

const emitErrorsAsWarnings =
  process.env.NODE_ENV === "development" && process.env.ESLINT_NO_DEV_ERRORS === "true"

function isError(message: Linter.LintMessage) {
  if (message.fatal || message.severity === 2) {
    return true
  }
  return false
}

function getRelativePath(filePath: string) {
  return path.relative(cwd, filePath)
}

export default function formatter(results: CLIEngine.LintResult[]): string {
  let output = "\n"
  let hasErrors = false
  let reportContainsErrorRuleIDs = false

  results.forEach(result => {
    if (result.messages.length === 0) {
      return
    }

    let messages = result.messages.map(message => {
      let messageType: string
      if (isError(message) && !emitErrorsAsWarnings) {
        messageType = "error"
        hasErrors = true
        if (message.ruleId) {
          reportContainsErrorRuleIDs = true
        }
      } else {
        messageType = "warn"
      }

      let line = message.line || "0"
      if (message.column) {
        line += ":" + message.column
      }
      const position = bold("Line " + line + ":")
      return [
        "",
        position,
        messageType,
        message.message.replace(/\.$/, ""),
        underline(message.ruleId || ""),
      ]
    })

    // if there are error messages, we want to show only errors
    if (hasErrors) {
      messages = messages.filter(m => m[2] === "error")
    }

    // add color to rule keywords
    messages.forEach(m => {
      m[4] = m[2] === "error" ? red(m[4]) : yellow(m[4])
      m.splice(2, 1)
    })

    const outputTable = table(messages, {
      align: ["l", "l", "l"],
      stringLength(str) {
        return stripAnsi(str).length
      },
    })

    // print the filename and relative path
    output += `${getRelativePath(result.filePath)}\n`

    // print the errors
    output += `${outputTable}\n\n`
  })

  if (reportContainsErrorRuleIDs) {
    // Unlike with warnings, we have to do it here.
    // We have similar code in react-scripts for warnings,
    // but warnings can appear in multiple files so we only
    // print it once at the end. For errors, however, we print
    // it here because we always show at most one error, and
    // we can only be sure it's an ESLint error before exiting
    // this function.
    output +=
      "Search for the " + underline(red("keywords")) + " to learn more about each error."
  }

  return output
}

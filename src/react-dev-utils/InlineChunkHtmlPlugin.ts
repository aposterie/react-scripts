/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import type { Plugin, Compiler } from "webpack"
import type HtmlWebpackPlugin from "html-webpack-plugin"

export default class InlineChunkHtmlPlugin implements Plugin {
  constructor(
    private htmlWebpackPlugin: typeof HtmlWebpackPlugin,
    private tests: RegExp[]
  ) {}

  private getInlinedTag(
    publicPath: string,
    assets: any,
    tag: HtmlWebpackPlugin.HtmlTagObject
  ): HtmlWebpackPlugin.HtmlTagObject {
    if (tag.tagName !== "script" || !tag.attributes?.src) {
      return tag
    }

    const scriptName = publicPath
      ? (tag.attributes.src as string).replace(publicPath, "")
      : (tag.attributes.src as string)

    if (!this.tests.some(test => scriptName.match(test))) {
      return tag
    }
    const asset = assets[scriptName]
    if (asset == null) {
      return tag
    }

    return {
      tagName: "script",
      innerHTML: asset.source(),
      closeTag: true,
    }
  }

  apply(compiler: Compiler) {
    let publicPath = compiler.options.output!.publicPath || ""
    if (publicPath && !publicPath.endsWith("/")) {
      publicPath += "/"
    }

    compiler.hooks.compilation.tap("InlineChunkHtmlPlugin", compilation => {
      const tagFunction = (tag: HtmlWebpackPlugin.HtmlTagObject) =>
        this.getInlinedTag(publicPath, compilation.assets, tag)

      const hooks = this.htmlWebpackPlugin.getHooks(compilation)
      hooks.alterAssetTagGroups.tap("InlineChunkHtmlPlugin", assets => {
        assets.headTags = assets.headTags.map(tagFunction)
        assets.bodyTags = assets.bodyTags.map(tagFunction)
        return assets
      })

      // Still emit the runtime chunk for users who do not use our generated
      // index.html file.
      // hooks.afterEmit.tap('InlineChunkHtmlPlugin', () => {
      //   Object.keys(compilation.assets).forEach(assetName => {
      //     if (this.tests.some(test => assetName.match(test))) {
      //       delete compilation.assets[assetName];
      //     }
      //   });
      // });
    })
  }
}

/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import address from "address"
import * as fs from "fs"
import * as path from "path"
import * as url from "url"
import { bold, cyan, green, yellow, red, underline } from "./chalk"
import detect from "detect-port-alt"
import isRoot from "is-root"
import prompts from "prompts"
import clearConsole from "./clearConsole"
import formatWebpackMessages from "./formatWebpackMessages"
import getProcessForPort from "./getProcessForPort"
import typescriptFormatter from "./typescriptFormatter"
import forkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin"
import { Deferred } from "./defer"
import type { IncomingMessage, ServerResponse } from "http"
import type { Compiler, Configuration } from "webpack"
import type WebpackDevServer from "webpack-dev-server"
import type { Issue } from "fork-ts-checker-webpack-plugin/lib/issue"

const { log } = console

const isInteractive = process.stdout.isTTY

export function prepareUrls(
  protocol: string,
  host: string,
  port: number,
  pathname = "/"
) {
  const formatUrl = (hostname: string) =>
    url.format({
      protocol,
      hostname,
      port,
      pathname,
    })

  const prettyPrintUrl = (hostname: string) =>
    url.format({
      protocol,
      hostname,
      port: bold(port),
      pathname,
    })

  const isUnspecifiedHost = host === "0.0.0.0" || host === "::"
  let prettyHost: string
  let lanUrlForConfig: string | undefined
  let lanUrlForTerminal: string | undefined
  if (isUnspecifiedHost) {
    prettyHost = "localhost"
    try {
      // This can only return an IPv4 address
      lanUrlForConfig = address.ip()
      if (lanUrlForConfig) {
        // Check if the address is a private ip
        // https://en.wikipedia.org/wiki/Private_network#Private_IPv4_address_spaces
        if (
          /^10[.]|^172[.](1[6-9]|2[0-9]|3[0-1])[.]|^192[.]168[.]/.test(lanUrlForConfig)
        ) {
          // Address is private, format it for later use
          lanUrlForTerminal = prettyPrintUrl(lanUrlForConfig)
        } else {
          // Address is not private, so we will discard it
          lanUrlForConfig = undefined
        }
      }
    } catch (_e) {
      // ignored
    }
  } else {
    prettyHost = host
  }

  const localUrlForTerminal = prettyPrintUrl(prettyHost)
  const localUrlForBrowser = formatUrl(prettyHost)
  return {
    lanUrlForConfig,
    lanUrlForTerminal,
    localUrlForTerminal,
    localUrlForBrowser,
  }
}

function printInstructions(
  appName: string,
  urls: ReturnType<typeof prepareUrls>,
  useYarn: boolean
) {
  log()
  log(`You can now view ${bold(appName)} in the browser.`)
  log()

  if (urls.lanUrlForTerminal) {
    log(`  ${bold("Local:")}            ${urls.localUrlForTerminal}`)
    log(`  ${bold("On Your Network:")}  ${urls.lanUrlForTerminal}`)
  } else {
    log(`  ${urls.localUrlForTerminal}`)
  }

  log()
  log("Note that the development build is not optimized.")
  log(
    `To create a production build, use ` +
      `${cyan(`${useYarn ? "yarn" : "npm run"} build`)}.`
  )
  log()
}

export function createCompiler({
  appName,
  config,
  devSocket,
  urls,
  useYarn,
  useTypeScript,
  tscCompileOnError,
  webpack,
}: {
  appName: string
  config: Configuration
  devSocket: {
    warnings(warnings: string[]): void
    errors(errors: string[]): void
  }
  urls: ReturnType<typeof prepareUrls>
  useYarn: boolean
  useTypeScript: boolean
  tscCompileOnError: boolean
  webpack: typeof import("webpack")
}): Compiler {
  // "Compiler" is a low-level interface to webpack.
  // It lets us listen to some events and provide our own custom messages.
  let compiler: Compiler
  try {
    compiler = webpack(config)
  } catch (err) {
    log(red("Failed to compile."))
    log()
    log(err.message || err)
    log()
    process.exit(1)
  }

  // "invalid" event fires when you have changed a file, and webpack is
  // recompiling a bundle. WebpackDevServer takes care to pause serving the
  // bundle, so if you refresh, it'll wait instead of serving the old one.
  // "invalid" is short for "bundle invalidated", it doesn't imply any errors.
  compiler.hooks.invalid.tap("invalid", () => {
    if (isInteractive) {
      clearConsole()
    }
    log("Compiling...")
  })

  let isFirstCompile = true
  let tsMessagesPromise: Deferred<{
    errors: string[]
    warnings: string[]
  }>

  if (useTypeScript) {
    compiler.hooks.beforeCompile.tap("beforeCompile", () => {
      tsMessagesPromise = new Deferred()
    })

    forkTsCheckerWebpackPlugin
      .getCompilerHooks(compiler)
      .issues.tap("afterTypeScriptCheck", allMsgs => {
        const format = (message: Issue) =>
          `${message.file}\n${typescriptFormatter(message)}`

        tsMessagesPromise.resolve({
          errors: allMsgs.filter(msg => msg.severity === "error").map(format),
          warnings: allMsgs.filter(msg => msg.severity === "warning").map(format),
        })
        return undefined!
      })
  }

  // "done" event fires when webpack has finished recompiling the bundle.
  // Whether or not you have warnings or errors, you will get this event.
  compiler.hooks.done.tap("done", async stats => {
    if (isInteractive) {
      clearConsole()
    }

    // We have switched off the default webpack output in WebpackDevServer
    // options so we are going to "massage" the warnings and errors and present
    // them in a readable focused way.
    // We only construct the warnings and errors for speed:
    // https://github.com/facebook/create-react-app/issues/4492#issuecomment-421959548
    const statsData = stats.toJson({
      all: false,
      warnings: true,
      errors: true,
    })

    if (useTypeScript && statsData.errors.length === 0) {
      const delayedMsg = setTimeout(() => {
        log(yellow("Files successfully emitted, waiting for typecheck results..."))
      }, 100)

      const messages = await tsMessagesPromise
      clearTimeout(delayedMsg)
      if (tscCompileOnError) {
        statsData.warnings.push(...messages.errors)
      } else {
        statsData.errors.push(...messages.errors)
      }
      statsData.warnings.push(...messages.warnings)

      // Push errors and warnings into compilation result
      // to show them after page refresh triggered by user.
      if (tscCompileOnError) {
        stats.compilation.warnings.push(...messages.errors)
      } else {
        stats.compilation.errors.push(...messages.errors)
      }
      stats.compilation.warnings.push(...messages.warnings)

      if (messages.errors.length > 0) {
        if (tscCompileOnError) {
          devSocket.warnings(messages.errors)
        } else {
          devSocket.errors(messages.errors)
        }
      } else if (messages.warnings.length > 0) {
        devSocket.warnings(messages.warnings)
      }

      if (isInteractive) {
        clearConsole()
      }
    }

    const messages = formatWebpackMessages(statsData)
    const isSuccessful = !messages.errors.length && !messages.warnings.length
    if (isSuccessful) {
      log(green("Compiled successfully!"))
    }
    if (isSuccessful && (isInteractive || isFirstCompile)) {
      printInstructions(appName, urls, useYarn)
    }
    isFirstCompile = false

    // If errors exist, only show errors.
    if (messages.errors.length) {
      // Only keep the first error. Others are often indicative
      // of the same problem, but confuse the reader with noise.
      if (messages.errors.length > 1) {
        messages.errors.length = 1
      }
      log(red("Failed to compile.\n"))
      log(messages.errors.join("\n\n"))
      return
    }

    // Show warnings if no errors were found.
    if (messages.warnings.length) {
      log(yellow("Compiled with warnings.\n"))
      log(messages.warnings.join("\n\n"))

      // Teach some ESLint tricks.
      log(
        `\nSearch for the ${underline(
          yellow("keywords")
        )} to learn more about each warning.`
      )
      log(`To ignore, add ${cyan("// eslint-disable-next-line")} to the line before.\n`)
    }
  })

  // You can safely remove this after ejecting.
  // We only use this block for testing of Create React App itself:
  const isSmokeTest = process.argv.some(arg => arg.indexOf("--smoke-test") > -1)
  if (isSmokeTest) {
    compiler.hooks.failed.tap("smokeTest", async () => {
      await tsMessagesPromise
      process.exit(1)
    })
    compiler.hooks.done.tap("smokeTest", async stats => {
      await tsMessagesPromise
      if (stats.hasErrors() || stats.hasWarnings()) {
        process.exit(1)
      } else {
        process.exit(0)
      }
    })
  }

  return compiler
}

function resolveLoopback(proxy: string): string {
  const o = url.parse(proxy)
  o.host = undefined!
  if (o.hostname !== "localhost") {
    return proxy
  }
  // Unfortunately, many languages (unlike node) do not yet support IPv6.
  // This means even though localhost resolves to ::1, the application
  // must fall back to IPv4 (on 127.0.0.1).
  // We can re-enable this in a few years.
  /*try {
    o.hostname = address.ipv6() ? '::1' : '127.0.0.1';
  } catch (_ignored) {
    o.hostname = '127.0.0.1';
  }*/

  try {
    // Check if we're on a network; if we are, chances are we can resolve
    // localhost. Otherwise, we can just be safe and assume localhost is
    // IPv4 for maximum compatibility.
    if (!address.ip()) {
      o.hostname = "127.0.0.1"
    }
  } catch (_ignored) {
    o.hostname = "127.0.0.1"
  }
  return url.format(o)
}

// We need to provide a custom onError function for httpProxyMiddleware.
// It allows us to log custom error messages on the console.
function onProxyError(proxy: string) {
  return (err: Error & { code?: number }, req: IncomingMessage, res: ServerResponse) => {
    const host = req.headers?.host
    log(
      `${red("Proxy error:")} Could not proxy request ${cyan(req.url)} from ${cyan(
        host
      )} to ${cyan(proxy)}.`
    )
    log(
      `See https://nodejs.org/api/errors.html#errors_common_system_errors for more information (${cyan(
        err.code
      )}).`
    )
    log()

    // And immediately send the proper error response to the client.
    // Otherwise, the request will eventually timeout with ERR_EMPTY_RESPONSE on the client side.
    if (res.writeHead && !res.headersSent) {
      res.writeHead(500)
    }
    res.end(
      `Proxy error: Could not proxy request ${req.url} from ${host} to ${proxy} (${err.code}).`
    )
  }
}

export function prepareProxy(
  proxy: string,
  appPublicFolder: string,
  servedPathname: string
): WebpackDevServer.ProxyConfigArray | undefined {
  // `proxy` lets you specify alternate servers for specific requests.
  if (!proxy) {
    return undefined
  }

  if (typeof proxy !== "string") {
    log(red('When specified, "proxy" in package.json must be a string.'))
    log(red('Instead, the type of "proxy" was "' + typeof proxy + '".'))
    log(red('Either remove "proxy" from package.json, or make it a string.'))
    process.exit(1)
  }

  // If proxy is specified, let it handle any request except for
  // files in the public folder and requests to the WebpackDevServer socket endpoint.
  // https://github.com/facebook/create-react-app/issues/6720
  const sockPath = process.env.WDS_SOCKET_PATH || "/sockjs-node"
  const isDefaultSockHost = !process.env.WDS_SOCKET_HOST
  function mayProxy(pathname: string) {
    const maybePublicPath = path.resolve(
      appPublicFolder,
      pathname.replace(new RegExp("^" + servedPathname), "")
    )
    const isPublicFileRequest = fs.existsSync(maybePublicPath)
    // used by webpackHotDevClient
    const isWdsEndpointRequest = isDefaultSockHost && pathname.startsWith(sockPath)
    return !(isPublicFileRequest || isWdsEndpointRequest)
  }

  if (!/^http(s)?:\/\//.test(proxy)) {
    log(
      red(
        'When "proxy" is specified in package.json it must start with either http:// or https://'
      )
    )
    process.exit(1)
  }

  let target: string
  if (process.platform === "win32") {
    target = resolveLoopback(proxy)
  } else {
    target = proxy
  }
  return [
    {
      target,
      logLevel: "silent",
      // For single page apps, we generally want to fallback to /index.html.
      // However we also want to respect `proxy` for API calls.
      // So if `proxy` is specified as a string, we need to decide which fallback to use.
      // We use a heuristic: We want to proxy all the requests that are not meant
      // for static assets and as all the requests for static assets will be using
      // `GET` method, we can proxy all non-`GET` requests.
      // For `GET` requests, if request `accept`s text/html, we pick /index.html.
      // Modern browsers include text/html into `accept` header when navigating.
      // However API calls like `fetch()` won’t generally accept text/html.
      // If this heuristic doesn’t work well for you, use `src/setupProxy.js`.
      context(pathname, req) {
        return (
          req.method !== "GET" ||
          (mayProxy(pathname) && req.headers.accept?.includes("text/html")) ||
          false
        )
      },
      onProxyReq(proxyReq) {
        // Browsers may send Origin headers even with same-origin
        // requests. To prevent CORS issues, we have to change
        // the Origin to match the target URL.
        if (proxyReq.getHeader("origin")) {
          proxyReq.setHeader("origin", target)
        }
      },
      onError: onProxyError(target),
      secure: false,
      changeOrigin: true,
      ws: true,
      xfwd: true,
    },
  ]
}

export async function choosePort(hostname: string, defaultPort: number) {
  try {
    const port = await detect(defaultPort)
    if (port === defaultPort) {
      return port
    }

    const message =
      process.platform !== "win32" && defaultPort < 1024 && !isRoot()
        ? `Admin permissions are required to run a server on a port below 1024.`
        : `Something is already running on port ${defaultPort}.`

    if (isInteractive) {
      clearConsole()
      const existingProcess = getProcessForPort(defaultPort)
      const answer = await prompts({
        type: "confirm",
        name: "shouldChangePort",
        message:
          yellow(
            message + `${existingProcess ? ` Probably:\n  ${existingProcess}` : ""}`
          ) + "\n\nWould you like to run the app on another port instead?",
        initial: true,
      })

      if (answer.shouldChangePort) {
        return port
      } else {
        return null
      }
    } else {
      log(red(message))
      return null
    }
  } catch (err) {
    throw Error(
      red(`Could not find an open port at ${bold(hostname)}.`) +
        "\n" +
        ("Network error message: " + err.message || err) +
        "\n"
    )
  }
}

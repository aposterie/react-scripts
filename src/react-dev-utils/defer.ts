type MaybePromise<T> = T | PromiseLike<T>

export interface Deferred<T> extends Promise<T> {
  [Symbol.toStringTag]: "Promise"
  resolve: (value?: MaybePromise<T>) => void
  reject: (reason?: any) => void
}

export class Deferred<T = void> implements Promise<T> {
  isSettled = Status.Pending
  value?: T

  constructor() {
    const promise = new Promise<T>((resolve, reject) => {
      this.resolve = async raw => {
        const value = await raw
        this.isSettled = Status.Resolved
        this.value = value
        resolve(value)
      }
      this.reject = reason => {
        this.isSettled = Status.Rejected
        reject(reason)
      }
    })
    this.then = promise.then.bind(promise)
    this.catch = promise.catch.bind(promise)
    this.finally = promise.finally.bind(promise)
  }
}

const enum Status {
  Pending,
  Resolved,
  Rejected,
}

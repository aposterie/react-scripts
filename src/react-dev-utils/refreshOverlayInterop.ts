import { dismissRuntimeErrors, reportRuntimeError } from "react-error-overlay"

export default {
  clearRuntimeErrors: dismissRuntimeErrors,
  handleRuntimeError: reportRuntimeError,
}

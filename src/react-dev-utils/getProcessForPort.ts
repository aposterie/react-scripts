/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import { cyan, grey, blue } from "./chalk"
import { execSync, ExecSyncOptionsWithStringEncoding } from "child_process"
import * as path from "path"

const execOptions: ExecSyncOptionsWithStringEncoding = {
  encoding: "utf8",
  stdio: [
    "pipe", // stdin (default)
    "pipe", // stdout (default)
    "ignore", //stderr
  ],
}

function isProcessAReactApp(processCommand: string) {
  return /^node .*react-scripts\/scripts\/start\.js\s?$/.test(processCommand)
}

function getProcessIdOnPort(port: number) {
  return execSync("lsof -i:" + port + " -P -t -sTCP:LISTEN", execOptions)
    .split("\n")[0]
    .trim()
}

function getPackageNameInDirectory(directory: string) {
  const packagePath = path.join(directory.trim(), "package.json")

  try {
    return require(packagePath).name
  } catch (e) {
    return null
  }
}

function getProcessCommand(processId: string, processDirectory: string) {
  let command = execSync("ps -o command -p " + processId + " | sed -n 2p", execOptions)

  command = command.replace(/\n$/, "")

  if (isProcessAReactApp(command)) {
    const packageName = getPackageNameInDirectory(processDirectory)
    return packageName ? packageName : command
  } else {
    return command
  }
}

function getDirectoryOfProcessById(processId: string) {
  return execSync(
    "lsof -p " +
      processId +
      ' | awk \'$4=="cwd" {for (i=9; i<=NF; i++) printf "%s ", $i}\'',
    execOptions
  ).trim()
}

export default function getProcessForPort(port: number) {
  try {
    const processId = getProcessIdOnPort(port)
    const directory = getDirectoryOfProcessById(processId)
    const command = getProcessCommand(processId, directory)
    return (
      cyan(command) + grey(" (pid " + processId + ")\n") + blue("  in ") + cyan(directory)
    )
  } catch (e) {
    return null
  }
}

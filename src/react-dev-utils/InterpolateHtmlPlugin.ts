/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

// This webpack plugin lets us interpolate custom variables into `index.html`.
// Usage: `new InterpolateHtmlPlugin(HtmlWebpackPlugin, { 'MY_VARIABLE': 42 })`
// Then, you can use %MY_VARIABLE% in your `index.html`.

// It works in tandem with HtmlWebpackPlugin.
// Learn more about creating plugins like this:
// https://github.com/ampedandwired/html-webpack-plugin#events

import type { Plugin, Compiler } from "webpack"
import type HtmlWebpackPlugin from "html-webpack-plugin"

import { forEach } from "lodash"
import escapeStringRegexp from "escape-string-regexp"

export default class InterpolateHtmlPlugin implements Plugin {
  constructor(
    private htmlWebpackPlugin: typeof HtmlWebpackPlugin,
    private replacements: Record<string, string | undefined>
  ) {}

  apply(compiler: Compiler) {
    compiler.hooks.compilation.tap("InterpolateHtmlPlugin", compilation => {
      this.htmlWebpackPlugin
        .getHooks(compilation)
        .afterTemplateExecution.tap("InterpolateHtmlPlugin", data => {
          // Run HTML through a series of user-specified string replacements.
          forEach(this.replacements, (value, key) => {
            if (value == null) return
            data.html = data.html.replace(
              RegExp("%" + escapeStringRegexp(key) + "%", "g"),
              value
            )
          })
          return data
        })
    })
  }
}

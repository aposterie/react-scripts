/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import { yellow, green, dim, cyan } from "./chalk"
import * as url from "url"
import globalModules from "global-modules"
import * as fs from "fs"

const { log } = console

export default function printHostingInstructions(
  appPackage: { scripts?: Record<string, string> },
  publicUrl: string,
  publicPath: string,
  buildFolder: string,
  useYarn: boolean
) {
  if (publicUrl && publicUrl.includes(".github.io/")) {
    // "homepage": "http://user.github.io/project"
    const publicPathname = url.parse(publicPath).pathname
    const hasDeployScript = appPackage.scripts?.deploy !== undefined
    printBaseMessage(buildFolder, publicPathname)

    printDeployInstructions(publicUrl, hasDeployScript, useYarn)
  } else if (publicPath !== "/") {
    // "homepage": "http://mywebsite.com/project"
    printBaseMessage(buildFolder, publicPath)
  } else {
    // "homepage": "http://mywebsite.com"
    //   or no homepage
    printBaseMessage(buildFolder, publicUrl)

    printStaticServerInstructions(buildFolder, useYarn)
  }
  log()
  log("Find out more about deployment here:")
  log()
  log(`  ${yellow("https://cra.link/deployment")}`)
  log()
}

function printBaseMessage(buildFolder: string, hostingLocation: null | string) {
  log(
    `The project was built assuming it is hosted at ${green(
      hostingLocation || "the server root"
    )}.`
  )
  log(
    `You can control this with the ${green("homepage")} field in your ${cyan(
      "package.json"
    )}.`
  )

  if (!hostingLocation) {
    log("For example, add this to build it for GitHub Pages:")
    log()

    log(
      `  ${green('"homepage"')} ${cyan(":")} ${green(
        '"http://myname.github.io/myapp"'
      )}${cyan(",")}`
    )
  }
  log()
  log(`The ${cyan(buildFolder)} folder is ready to be deployed.`)
}

function printDeployInstructions(
  publicUrl: string,
  hasDeployScript: boolean,
  useYarn: boolean
) {
  log(`To publish it at ${green(publicUrl)} , run:`)
  log()

  // If script deploy has been added to package.json, skip the instructions
  if (!hasDeployScript) {
    if (useYarn) {
      log(`  ${cyan("yarn")} add --dev gh-pages`)
    } else {
      log(`  ${cyan("npm")} install --save-dev gh-pages`)
    }
    log()

    log(`Add the following script in your ${cyan("package.json")}.`)
    log()

    log(`    ${dim("// ...")}`)
    log(`    ${yellow('"scripts"')}: {`)
    log(`      ${dim("// ...")}`)
    log(
      `      ${yellow('"predeploy"')}: ${yellow(
        `"${useYarn ? "yarn" : "npm run"} build",`
      )}`
    )
    log(`      ${yellow('"deploy"')}: ${yellow('"gh-pages -d build"')}`)
    log("    }")
    log()

    log("Then run:")
    log()
  }
  log(`  ${cyan(useYarn ? "yarn" : "npm")} run deploy`)
}

function printStaticServerInstructions(buildFolder: string, useYarn: boolean) {
  log("You may serve it with a static server:")
  log()

  if (!fs.existsSync(`${globalModules}/serve`)) {
    if (useYarn) {
      log(`  ${cyan("yarn")} global add serve`)
    } else {
      log(`  ${cyan("npm")} install -g serve`)
    }
  }
  log(`  ${cyan("serve")} -s ${buildFolder}`)
}

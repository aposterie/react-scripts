/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import * as fs from "fs"
import * as path from "path"
import { red, cyan } from "./chalk"

export default function checkRequiredFiles(files: string[]) {
  let currentFilePath = ""
  try {
    files.forEach(filePath => {
      currentFilePath = filePath
      fs.accessSync(filePath, fs.constants.F_OK)
    })
    return true
  } catch (err) {
    const dirName = path.dirname(currentFilePath)
    const fileName = path.basename(currentFilePath)
    console.log(red("Could not find a required file."))
    console.log(red("  Name: ") + cyan(fileName))
    console.log(red("  Searched in: ") + cyan(dirName))
    return false
  }
}

/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import { loadConfig, clearCaches } from "browserslist"
import { yellow, bold, red, underline, green, cyan } from "../react-dev-utils/chalk"
import { EOL } from "os"
import prompts from "prompts"
import pkgUp from "pkg-up"
import * as fs from "fs-extra"

export const defaultBrowsers = {
  production: [">0.2%", "not dead", "not op_mini all", "not IE > 0"],
  development: [
    "last 1 chrome version",
    "last 1 firefox version",
    "last 1 safari version",
  ],
}

export async function shouldSetBrowsers(isInteractive: boolean) {
  if (!isInteractive) {
    return true
  }

  const answer = await prompts({
    type: "confirm",
    name: "shouldSetBrowsers",
    message:
      yellow("We are unable to detect target browsers.") +
      `\n\nWould you like to add the defaults to your ${bold("package.json")}?`,
    initial: true,
  })

  return answer.shouldSetBrowsers as boolean
}

export async function checkBrowsers(
  dir: string,
  isInteractive: boolean,
  retry = true
): Promise<void> {
  const current = loadConfig({ path: dir })
  if (current != null) {
    return current
  }

  if (!retry) {
    throw Error(
      red("As of react-scripts >=2 you must specify targeted browsers.") +
        `\nPlease add a ${underline("browserslist")} key to your ${bold("package.json")}.`
    )
  }

  if (!(await shouldSetBrowsers(isInteractive))) {
    return checkBrowsers(dir, isInteractive, false)
  }

  try {
    const filePath = await pkgUp({ cwd: dir })
    if (filePath == null) {
      throw Error("filePath is null")
    }
    const pkg = await fs.readJSON(filePath)
    pkg.browserslist = defaultBrowsers
    await fs.writeFile(filePath, JSON.stringify(pkg, null, 2) + EOL)

    clearCaches()
    console.log()
    console.log(`${green("Set target browsers:")} ${cyan(defaultBrowsers)}`)
    console.log()
    // Swallow any error
  } catch {}

  await checkBrowsers(dir, isInteractive, false)
}

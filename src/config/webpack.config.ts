import * as fs from "fs"
import * as path from "path"
import * as webpack from "webpack"
import { __importDefault } from "tslib"
import PnpWebpackPlugin from "pnp-webpack-plugin"
import HtmlWebpackPlugin from "html-webpack-plugin"
import CaseSensitivePathsPlugin from "case-sensitive-paths-webpack-plugin"
import InlineChunkHtmlPlugin from "../react-dev-utils/InlineChunkHtmlPlugin"
import TerserPlugin from "terser-webpack-plugin"
import MiniCssExtractPlugin from "mini-css-extract-plugin"
import OptimizeCSSAssetsPlugin from "optimize-css-assets-webpack-plugin"
import safePostCssParser from "postcss-safe-parser"
import { WebpackManifestPlugin } from "webpack-manifest-plugin"
import InterpolateHtmlPlugin from "../react-dev-utils/InterpolateHtmlPlugin"
import WorkerPlugin from "worker-plugin"
import { InjectManifest } from "workbox-webpack-plugin"
import WatchMissingNodeModulesPlugin from "../react-dev-utils/WatchMissingNodeModulesPlugin"
import ModuleScopePlugin from "../react-dev-utils/ModuleScopePlugin"
import ESLintPlugin from "eslint-webpack-plugin"
import getCSSModuleLocalIdent from "../react-dev-utils/getCSSModuleLocalIdent"
import createPaths, { moduleFileExtensions } from "./paths"
import modules from "./modules"
import getClientEnvironment from "./env"
import ModuleNotFoundPlugin from "../react-dev-utils/ModuleNotFoundPlugin"
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin"
import ReactRefreshWebpackPlugin from "@pmmmwh/react-refresh-webpack-plugin"
import postcssNormalize from "postcss-normalize"
import eslintFormatter from "../react-dev-utils/eslintFormatter"
import { requireTSNode } from "../react-dev-utils/nodeHook"

const babelLoader = require.resolve("babel-loader")
const cssLoader = require.resolve("css-loader")
const sassLoader = require.resolve("sass-loader")
const postcssLoader = require.resolve("postcss-loader")
const urlLoader = require.resolve("url-loader")
const fileLoader = require.resolve("file-loader")
import postcssPresetEnv from "postcss-preset-env"

import babelPresetReactApp from "../babel-preset-react-app/index"
import dependencies from "../babel-preset-react-app/dependencies"
const webpackOverrides = require.resolve("../babel-preset-react-app/webpack-overrides")
import postcssFlexBugsFixes from "postcss-flexbugs-fixes"

const paths = createPaths()

const appPackageJson = require(paths.appPackageJson)

// Source maps are resource heavy and can cause out of memory issue for large source files.
const shouldUseSourceMap = process.env.GENERATE_SOURCEMAP !== "false"

const webpackDevClientEntry = require.resolve("../react-dev-utils/webpackHotDevClient")

const reactRefreshOverlayEntry = require.resolve(
  "../react-dev-utils/refreshOverlayInterop"
)

// Some apps do not need the benefits of saving a web request, so not inlining the chunk
// makes for a smoother build process.
const shouldInlineRuntimeChunk = process.env.INLINE_RUNTIME_CHUNK !== "false"

const emitErrorsAsWarnings = process.env.ESLINT_NO_DEV_ERRORS === "true"
const disableESLintPlugin = process.env.DISABLE_ESLINT_PLUGIN === "true"

const NO_TYPECHECK = !!process.env.NO_TYPECHECK
const NO_TERSER = !!process.env.NO_TERSER

const imageInlineSizeLimit = parseInt(process.env.IMAGE_INLINE_SIZE_LIMIT || "10000")

// Check if TypeScript is setup
const useTypeScript = fs.existsSync(paths.appTsConfig)

// Get the path to the uncompiled service worker (if it exists).
const swSrc = paths.swSrc

// style files regexes
const cssRegex = /\.css$/
const cssModuleRegex = /\.module\.css$/
const sassRegex = /\.(scss|sass)$/
const sassModuleRegex = /\.module\.(scss|sass)$/

const hasJsxRuntime = (() => {
  if (process.env.DISABLE_NEW_JSX_TRANSFORM === "true") {
    return false
  }

  try {
    require.resolve("react/jsx-runtime")
    return true
  } catch (e) {
    return false
  }
})()

const never = (): never => {
  throw Error()
}

// This is the production and development configuration.
// It is focused on developer experience, fast rebuilds, and a minimal bundle.

export default async function createConfig(
  webpackEnv: "development" | "production"
): Promise<webpack.Configuration> {
  const DEV = webpackEnv === "development" || undefined
  const PROD = webpackEnv === "production" || undefined

  const NODE_ENV = PROD ? "production" : DEV ? "development" : never()

  // Variable used for enabling profiling in Production
  // passed into alias object. Uses a flag if passed into the build command
  const isEnvProductionProfile = PROD && process.argv.includes("--profile")

  // We will provide `paths.publicUrlOrPath` to our app
  // as %PUBLIC_URL% in `index.html` and `process.env.PUBLIC_URL` in JavaScript.
  // Omit trailing slash as %PUBLIC_URL%/xyz looks better than %PUBLIC_URL%xyz.
  // Get environment variables to inject into our app.
  const env = getClientEnvironment(paths.publicUrlOrPath.slice(0, -1))

  const shouldUseReactRefresh = env.raw.FAST_REFRESH

  /**
   * common function to get style loaders
   */
  const getStyleLoaders = ({
    preprocessor,
    ...cssOptions
  }: {
    preprocessor?: any
    importLoaders: number
    sourceMap?: boolean
    modules?: {
      getLocalIdent: any
    }
  }): webpack.RuleSetUseItem[] => {
    const loaders: webpack.RuleSetUseItem[] = [
      DEV && require.resolve("style-loader"),
      PROD && {
        loader: MiniCssExtractPlugin.loader,
        // css is located in `static/css`, use '../../' to locate index.html folder
        // in production `paths.publicUrlOrPath` can be a relative path
        options: paths.publicUrlOrPath.startsWith(".") ? { publicPath: "../../" } : {},
      },
      {
        loader: cssLoader,
        options: cssOptions,
      },
      {
        // Options for PostCSS as we reference these options twice
        // Adds vendor prefixing based on your specified browser support in
        // package.json
        loader: postcssLoader,
        options: {
          postcssOptions: {
            plugins: [
              postcssFlexBugsFixes,
              postcssPresetEnv({
                autoprefixer: {
                  flexbox: "no-2009",
                },
                stage: 3,
              }),
              // Adds PostCSS Normalize as the reset css with default options,
              // so that it honors browserslist config in package.json
              // which in turn let's users customize the target behavior as per their needs.
              postcssNormalize(),
            ],
            sourceMap: PROD ? shouldUseSourceMap : DEV,
          },
        },
      },
    ].filter(Boolean)

    if (preprocessor) {
      loaders.push(
        {
          loader: require.resolve("resolve-url-loader"),
          options: {
            sourceMap: PROD ? shouldUseSourceMap : DEV,
            root: paths.appSrc,
          },
        },
        {
          loader: preprocessor,
          options: {
            sourceMap: true,
          },
        }
      )
    }
    return loaders
  }

  const config: webpack.Configuration = {
    mode: NODE_ENV,
    // Stop compilation early in production
    bail: PROD,
    devtool: PROD
      ? shouldUseSourceMap
        ? "source-map"
        : false
      : DEV && "cheap-module-source-map",
    // These are the "entry points" to our application.
    // This means they will be the "root" imports that are included in JS bundle.
    entry:
      DEV && !shouldUseReactRefresh
        ? [
            webpackDevClientEntry + "?/",
            require.resolve("webpack/hot/dev-server"),

            /**
             * Finally, this is your app's code:
             */
            paths.appIndexJs,

            /**
             * We include the app code last so that if there is a runtime error during
             * initialization, it doesn't blow up the WebpackDevServer client, and
             * changing JS code would still trigger a refresh.
             */
          ]
        : paths.appIndexJs,

    output: {
      // The build folder.
      path: PROD && paths.appBuild,
      /**
       * Add /* filename * / comments to generated require()s in the output.
       */
      pathinfo: DEV,
      // There will be one main bundle, and one file per asynchronous chunk.
      // In development, it does not produce real files.
      filename: PROD
        ? "static/js/[name].[contenthash:8].js"
        : DEV && "static/js/bundle.js",
      // There are also additional JS chunk files if you use code splitting.
      chunkFilename: PROD
        ? "static/js/[name].[contenthash:8].chunk.js"
        : DEV && "static/js/[name].chunk.js",

      /**
       * webpack uses `publicPath` to determine where the app is being served from.
       * It requires a trailing slash, or the file assets will get an incorrect path.
       * We inferred the "public path" (such as / or /my-project) from homepage.
       */
      publicPath: paths.publicUrlOrPath,

      /**
       * Point sourcemap entries to original disk location (format as URL on Windows)
       */
      devtoolModuleFilenameTemplate: PROD
        ? info =>
            path.relative(paths.appSrc, info.absoluteResourcePath).replace(/\\/g, "/")
        : DEV && (info => path.resolve(info.absoluteResourcePath).replace(/\\/g, "/")),

      /**
       * Prevents conflicts when multiple webpack runtimes (from different apps)
       * are used on the same page.
       */
      jsonpFunction: `webpackJsonp${appPackageJson.name}`,

      /**
       * this defaults to 'window', but by setting it to 'this' then
       * module chunks which are built will work in web workers as well.
       */
      globalObject: "this",
    },
    optimization: {
      minimize: PROD,
      minimizer: [
        // This is only used in production mode
        !NO_TERSER &&
          new TerserPlugin({
            terserOptions: {
              parse: {
                // We want terser to parse any ecma code. However, we don't want it
                // to apply any minification steps that turns valid ecma 5 code
                // into invalid ecma 5 code. This is why the 'compress' and 'output'
                // sections only apply transformations that are ecma 5 safe
                // https://github.com/facebook/create-react-app/pull/4234
                ecma: 2020,
              },
              compress: {
                ecma: 5,
                // Disabled because of an issue with Uglify breaking seemingly valid code:
                // https://github.com/facebook/create-react-app/issues/2376
                // Pending further investigation:
                // https://github.com/mishoo/UglifyJS2/issues/2011
                comparisons: false,
                // Disabled because of an issue with Terser breaking valid code:
                // https://github.com/facebook/create-react-app/issues/5250
                // Pending further investigation:
                // https://github.com/terser-js/terser/issues/120
                inline: 2,
              },
              mangle: {
                safari10: true,
              },
              // Added for profiling in devtools
              keep_classnames: isEnvProductionProfile,
              keep_fnames: isEnvProductionProfile,
              output: {
                ecma: 5,
                comments: false,
                // Turned on because emoji and regex is not minified properly using default
                // https://github.com/facebook/create-react-app/issues/2488
                ascii_only: true,
              },
              sourceMap: shouldUseSourceMap,
            },
          }),
        // This is only used in production mode
        new OptimizeCSSAssetsPlugin({
          cssProcessorOptions: {
            parser: safePostCssParser,
            map: shouldUseSourceMap
              ? {
                  // `inline: false` forces the sourcemap to be output into a
                  // separate file
                  inline: false,
                  // `annotation: true` appends the sourceMappingURL to the end of
                  // the css file, helping the browser find the sourcemap
                  annotation: true,
                }
              : false,
          },
          cssProcessorPluginOptions: {
            preset: ["default", { minifyFontValues: { removeQuotes: false } }],
          },
        }),
      ].filter(Boolean),
      // Automatically split vendor and commons
      // https://twitter.com/wSokra/status/969633336732905474
      // https://medium.com/webpack/webpack-4-code-splitting-chunk-graph-and-the-splitchunks-optimization-be739a861366
      splitChunks: {
        chunks: "all",
        name: DEV,
      },
      // Keep the runtime chunk separated to enable long term caching
      // https://twitter.com/wSokra/status/969679223278505985
      // https://github.com/facebook/create-react-app/issues/5358
      runtimeChunk: {
        name: entrypoint => `runtime-${entrypoint.name}`,
      },
    },
    resolve: {
      /**
       * This allows you to set a fallback for where webpack should look for modules.
       * We placed these paths second because we want `node_modules` to "win"
       * if there are any conflicts. This matches Node resolution mechanism.
       * https://github.com/facebook/create-react-app/issues/253
       */
      modules: ["node_modules", paths.appNodeModules].concat(
        modules.additionalModulePaths || []
      ),
      // These are the reasonable defaults supported by the Node ecosystem.
      // We also include JSX as a common component filename extension to support
      // some tools, although we do not recommend using it, see:
      // https://github.com/facebook/create-react-app/issues/290
      // `web` extension prefixes have been added for better support
      // for React Native Web.
      extensions: moduleFileExtensions
        .map(ext => `.${ext}`)
        .filter(ext => useTypeScript || !ext.includes("ts")),
      alias: {
        // Support React Native Web
        // https://www.smashingmagazine.com/2016/08/a-glimpse-into-the-future-with-react-native-for-web/
        "react-native": "react-native-web",
        // Allows for better profiling with ReactDevTools
        ...(isEnvProductionProfile && {
          "react-dom$": "react-dom/profiling",
          "scheduler/tracing": "scheduler/tracing-profiling",
        }),
        ...modules.webpackAliases,
      },
      plugins: [
        // Adds support for installing with Plug’n’Play, leading to faster installs and adding
        // guards against forgotten dependencies and such.
        PnpWebpackPlugin,
        // Prevents users from importing files from outside of src/ (or node_modules/).
        // This often causes confusion because we only process files within src/ with babel.
        // To fix this, we prevent you from importing files out of src/ -- if you'd like to,
        // please link the files into your node_modules/ and let module-resolution kick in.
        // Make sure your source files are compiled, as they will not be processed in any way.
        new ModuleScopePlugin(paths.appSrc, [
          paths.appPackageJson,
          reactRefreshOverlayEntry,
        ]),
      ],
    },
    resolveLoader: {
      plugins: [
        // Also related to Plug’n’Play, but this time it tells webpack to load its loaders
        // from the current package.
        PnpWebpackPlugin.moduleLoader(module),
      ],
    },
    module: {
      strictExportPresence: true,
      rules: [
        {
          // "oneOf" will traverse all following loaders until one will
          // match the requirements. When no loader matches it will fall
          // back to the "file" loader at the end of the loader list.
          oneOf: [
            // TODO: Merge this config once `image/avif` is in the mime-db
            // https://github.com/jshttp/mime-db
            {
              test: [/\.avif$/],
              loader: require.resolve("url-loader"),
              options: {
                limit: imageInlineSizeLimit,
                mimetype: "image/avif",
                name: "static/media/[name].[hash:8].[ext]",
              },
            },
            // "url" loader works like "file" loader except that it embeds assets
            // smaller than specified limit in bytes as data URLs to avoid requests.
            // A missing `test` is equivalent to a match.
            {
              test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
              loader: urlLoader,
              options: {
                limit: imageInlineSizeLimit,
                name: "static/media/[name].[hash:8].[ext]",
              },
            },
            // Process application JS with Babel.
            // The preset includes JSX, Flow, TypeScript, and some ESnext features.
            {
              test: /\.(js|mjs|jsx|ts|tsx)$/,
              include: paths.appSrc,
              loader: babelLoader,
              options: {
                customize: webpackOverrides,
                // @remove-on-eject-begin
                babelrc: false,
                configFile: false,
                presets: [
                  [
                    babelPresetReactApp,
                    {
                      runtime: hasJsxRuntime ? "automatic" : "classic",
                    },
                  ],
                ],
                // @remove-on-eject-end
                plugins: [
                  DEV && shouldUseReactRefresh && require.resolve("react-refresh/babel"),
                ].filter(Boolean),
                // This is a feature of `babel-loader` for webpack (not Babel itself).
                // It enables caching results in ./node_modules/.cache/babel-loader/
                // directory for faster rebuilds.
                cacheDirectory: true,
                // See #6846 for context on why cacheCompression is disabled
                cacheCompression: false,
                compact: PROD,
              },
            },
            // Process any JS outside of the app with Babel.
            // Unlike the application JS, we only compile the standard ES features.
            {
              test: /\.(js|mjs)$/,
              exclude: /@babel(?:\/|\\{1,2})runtime/,
              loader: babelLoader,
              options: {
                babelrc: false,
                configFile: false,
                compact: false,
                presets: [[dependencies, { helpers: true }]],
                cacheDirectory: true,
                // See #6846 for context on why cacheCompression is disabled
                cacheCompression: false,
                // Babel sourcemaps are needed for debugging into node_modules
                // code.  Without the options below, debuggers like VSCode
                // show incorrect code and set breakpoints on the wrong lines.
                sourceMaps: shouldUseSourceMap,
                inputSourceMap: shouldUseSourceMap,
              },
            },
            // "postcss" loader applies autoprefixer to our CSS.
            // "css" loader resolves paths in CSS and adds assets as dependencies.
            // "style" loader turns CSS into JS modules that inject <style> tags.
            // In production, we use MiniCSSExtractPlugin to extract that CSS
            // to a file, but in development "style" loader enables hot editing
            // of CSS.
            // By default we support CSS Modules with the extension .module.css
            {
              test: cssRegex,
              exclude: cssModuleRegex,
              use: getStyleLoaders({
                importLoaders: 1,
                sourceMap: PROD ? shouldUseSourceMap : DEV,
              }),
              // Don't consider CSS imports dead code even if the
              // containing package claims to have no side effects.
              // Remove this when webpack adds a warning or an error for this.
              // See https://github.com/webpack/webpack/issues/6571
              sideEffects: true,
            },
            // Adds support for CSS Modules (https://github.com/css-modules/css-modules)
            // using the extension .module.css
            {
              test: cssModuleRegex,
              use: getStyleLoaders({
                importLoaders: 1,
                sourceMap: PROD ? shouldUseSourceMap : DEV,
                modules: {
                  getLocalIdent: getCSSModuleLocalIdent,
                },
              }),
            },
            /**
             * Opt-in support for SASS (using .scss or .sass extensions).
             * By default we support SASS Modules with the
             * extensions .module.scss or .module.sass
             */
            {
              test: sassRegex,
              exclude: sassModuleRegex,
              use: getStyleLoaders({
                importLoaders: 3,
                preprocessor: sassLoader,
                sourceMap: PROD ? shouldUseSourceMap : DEV,
              }),
              // Don't consider CSS imports dead code even if the
              // containing package claims to have no side effects.
              // Remove this when webpack adds a warning or an error for this.
              // See https://github.com/webpack/webpack/issues/6571
              sideEffects: true,
            },
            // Adds support for CSS Modules, but using SASS
            // using the extension .module.scss or .module.sass
            {
              test: sassModuleRegex,
              use: getStyleLoaders({
                preprocessor: sassLoader,
                importLoaders: 3,
                sourceMap: PROD ? shouldUseSourceMap : DEV,
                modules: {
                  getLocalIdent: getCSSModuleLocalIdent,
                },
              }),
            },
            /**
             * "file" loader makes sure those assets get served by WebpackDevServer.
             * When you `import` an asset, you get its (virtual) filename.
             * In production, they would get copied to the `build` folder.
             * This loader doesn't use a "test" so it will catch all modules
             * that fall through the other loaders.
             */
            {
              loader: fileLoader,
              /**
               * Exclude `js` files to keep "css" loader working as it injects
               * its runtime that would otherwise be processed through "file" loader.
               * Also exclude `html` and `json` extensions so they get processed
               * by webpack’s internal loaders.
               */
              exclude: [/\.(js|mjs|jsx|ts|tsx)$/, /\.html$/, /\.json$/],
              options: {
                name: "static/media/[name].[hash:8].[ext]",
              },
            },
            // ** STOP ** Are you adding a new loader?
            // Make sure to add the new loader(s) before the "file" loader.
          ],
        },
      ],
    },
    plugins: [
      // Generates an `index.html` file with the <script> injected.
      new HtmlWebpackPlugin({
        inject: true,
        template: paths.appHtml,
        ...(PROD && {
          minify: {
            removeComments: true,
            collapseWhitespace: true,
            removeRedundantAttributes: true,
            useShortDoctype: true,
            removeEmptyAttributes: true,
            removeStyleLinkTypeAttributes: true,
            keepClosingSlash: true,
            minifyJS: true,
            minifyCSS: true,
            minifyURLs: true,
          },
        }),
      }),

      // Inlines the webpack runtime script. This script is too small to warrant
      // a network request.
      // https://github.com/facebook/create-react-app/issues/5358
      PROD &&
        shouldInlineRuntimeChunk &&
        new InlineChunkHtmlPlugin(HtmlWebpackPlugin, [/runtime-.+[.]js/]),

      /**
       * Makes some environment variables available in index.html.
       * The public URL is available as %PUBLIC_URL% in index.html, e.g.:
       * <link rel="icon" href="%PUBLIC_URL%/favicon.ico">
       * It will be an empty string unless you specify "homepage"
       * in `package.json`, in which case it will be the pathname of that URL.
       */
      new InterpolateHtmlPlugin(
        HtmlWebpackPlugin,
        (env.raw as any) as Record<string, string>
      ),

      /**
       * This gives some necessary context to module not found errors, such as
       * the requesting resource.
       */
      new ModuleNotFoundPlugin(paths.appPath),

      /**
       * Makes some environment variables available to the JS code, for example:
       * if (process.env.NODE_ENV === 'production') { ... }. See `./env.js`.
       * It is absolutely essential that NODE_ENV is set to production
       * during a production build.
       * Otherwise React will be compiled in the very slow development mode.
       */
      new webpack.DefinePlugin(env.stringified),

      // This is necessary to emit hot updates (currently CSS only):
      DEV && new webpack.HotModuleReplacementPlugin(),
      // Experimental hot reloading for React .
      // https://github.com/facebook/react/tree/master/packages/react-refresh
      DEV &&
        shouldUseReactRefresh &&
        new ReactRefreshWebpackPlugin({
          overlay: {
            entry: webpackDevClientEntry,
          },
        }),

      /**
       * Watcher doesn't work well if you mistype casing in a path so we use
       * a plugin that prints an error when you attempt to do this.
       * See https://github.com/facebook/create-react-app/issues/240
       */
      DEV && new CaseSensitivePathsPlugin(),

      /**
       * If you require a missing module and then `npm install` it, you still have
       * to restart the development server for webpack to discover it. This plugin
       * makes the discovery automatic so you don't have to restart.
       * See https://github.com/facebook/create-react-app/issues/186
       */
      DEV && new WatchMissingNodeModulesPlugin(paths.appNodeModules),

      PROD &&
        new MiniCssExtractPlugin({
          // Options similar to the same options in webpackOptions.output
          // both options are optional
          filename: "static/css/[name].[contenthash:8].css",
          chunkFilename: "static/css/[name].[contenthash:8].chunk.css",
        }),

      /**
       * Generate an asset manifest file with the following content:
       * - "files" key: Mapping of all asset filenames to their corresponding
       *   output file so that tools can pick it up without having to parse
       *   `index.html`
       * - "entrypoints" key: Array of files which are included in `index.html`,
       *   can be used to reconstruct the HTML if necessary
       */
      new WebpackManifestPlugin({
        fileName: "asset-manifest.json",
        publicPath: paths.publicUrlOrPath,
        generate(seed, files, entryPoints: any) {
          const manifestFiles = files.reduce((manifest, file) => {
            manifest[file.name!] = file.path
            return manifest
          }, seed as Record<string, string>)
          const entrypointFiles = entryPoints.main.filter(
            fileName => !fileName.endsWith(".map")
          )

          return {
            files: manifestFiles,
            entrypoints: entrypointFiles,
          }
        },
      }),

      // Moment.js is an extremely popular library that bundles large locale files
      // by default due to how webpack interprets its code. This is a practical
      // solution that requires the user to opt into importing specific locales.
      // https://github.com/jmblog/how-to-optimize-momentjs-with-webpack
      // You can remove this if you don't use Moment.js:
      new webpack.IgnorePlugin({
        resourceRegExp: /^\.\/locale$/,
        contextRegExp: /moment$/,
      }),

      // Generate a service worker script that will precache, and keep up to date,
      // the HTML & assets that are part of the webpack build.
      PROD &&
        fs.existsSync(swSrc) &&
        new InjectManifest({
          swSrc,
          dontCacheBustURLsMatching: /\.[0-9a-f]{8}\./,
          exclude: [/\.map$/, /asset-manifest\.json$/, /LICENSE/],
          // Bump up the default maximum size (2mb) that's precached,
          // to make lazy-loading failure scenarios less likely.
          // See https://github.com/cra-template/pwa/issues/13#issuecomment-722667270
          maximumFileSizeToCacheInBytes: 5 * 1024 * 1024,
        }),

      // TypeScript type checking
      useTypeScript &&
        !NO_TYPECHECK &&
        new ForkTsCheckerWebpackPlugin({
          async: DEV,
          typescript: {
            configFile: paths.appTsConfig,
          },
          // The formatter is invoked directly in WebpackDevServerUtils during development
          formatter: PROD && {
            type: "codeframe",
            options: {
              highlightCode: true,
            },
          },
        }),

      !disableESLintPlugin &&
        new ESLintPlugin({
          // Plugin options
          extensions: ["js", "mjs", "jsx", "ts", "tsx"],
          formatter: eslintFormatter,
          eslintPath: require.resolve("eslint"),
          emitWarning: DEV && emitErrorsAsWarnings,
          context: paths.appSrc,
          cache: true,
          cacheLocation: path.resolve(paths.appNodeModules, ".cache/.eslintcache"),
          // ESLint class options
          cwd: paths.appPath,
          resolvePluginsRelativeTo: __dirname,
          baseConfig: {
            parser: "@babel/eslint-parser",
            env: {
              es6: true,
              browser: true,
              commonjs: true,
              node: true,
            },
            parserOptions: {
              ecmaVersion: 2020,
              sourceType: "module",
              ecmaFeatures: {
                jsx: true,
              },
              requireConfigFile: false,
              babelOptions: {
                parserOpts: {
                  // https://babeljs.io/docs/en/babel-parser#ecmascript-proposalshttpsgithubcombabelproposals
                  plugins: [
                    "classPrivateMethods",
                    "classPrivateProperties",
                    "classProperties",
                    "classStaticBlock",
                    "decimal",
                    "decorators-legacy",
                    "exportDefaultFrom",
                    "importAssertions",
                    "jsx",
                    "moduleStringNames",
                    "privateIn",
                    "typescript",
                  ],
                },
                plugins: [],
              },
            },
            settings: {
              react: {
                version: "detect",
              },
            },
          },
        }),
      new WorkerPlugin(),
    ].filter(Boolean),
    // Some libraries import Node modules but don't use them in the browser.
    // Tell webpack to provide empty mocks for them so importing them works.
    node: {},
    // Turn off performance processing because we utilize
    // our own hints via the FileSizeReporter
    performance: false,

    stats: {
      errorDetails: true,
    },
  }

  // react-app-rewired support out-of-the-box
  if (fs.existsSync(paths.override)) {
    if (/\.tsx?/.test(paths.override)) {
      requireTSNode()
    }

    const override: {
      default(
        config: webpack.Configuration,
        env: string
      ): webpack.Configuration | Promise<webpack.Configuration>
    } = __importDefault(require(paths.override))
    return await override.default(config, NODE_ENV)
  }

  return config
}

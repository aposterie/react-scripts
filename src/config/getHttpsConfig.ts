import { existsSync, readFileSync } from "fs"
import { resolve } from "path"
import { publicEncrypt, privateDecrypt } from "crypto"
import { yellow, cyan } from "../react-dev-utils/chalk"
import createPaths from "./paths"

const paths = createPaths()

// Ensure the certificate and key provided are valid and if not
// throw an easy to debug error
function validateKeyAndCerts(
  cert: Buffer,
  key: Buffer,
  keyFile: string,
  crtFile: string
) {
  let encrypted: Buffer
  try {
    // publicEncrypt will throw an error with an invalid cert
    encrypted = publicEncrypt(cert, Buffer.from("test"))
  } catch (err) {
    throw Error(`The certificate "${yellow(crtFile)}" is invalid.\n${err.message}`)
  }

  try {
    // privateDecrypt will throw an error with an invalid key
    privateDecrypt(key, encrypted)
  } catch (err) {
    throw Error(`The certificate key "${yellow(keyFile)}" is invalid.\n${err.message}`)
  }
}

// Read file and throw an error if it doesn't exist
function readEnvFile(file: string, type: string) {
  if (!existsSync(file)) {
    throw Error(
      `You specified ${cyan(type)} in your env, but the file "${yellow(
        file
      )}" can't be found.`
    )
  }
  return readFileSync(file)
}

// Get the https config
// Return cert files if provided in env, otherwise just true or false
export default function getHttpsConfig() {
  const { SSL_CRT_FILE, SSL_KEY_FILE, HTTPS } = process.env
  const isHttps = HTTPS === "true"

  if (isHttps && SSL_CRT_FILE && SSL_KEY_FILE) {
    const crtFile = resolve(paths.appPath, SSL_CRT_FILE)
    const keyFile = resolve(paths.appPath, SSL_KEY_FILE)
    const config = {
      cert: readEnvFile(crtFile, "SSL_CRT_FILE"),
      key: readEnvFile(keyFile, "SSL_KEY_FILE"),
    }

    validateKeyAndCerts(config.cert, config.key, keyFile, crtFile)
    return config
  }
  return isHttps
}

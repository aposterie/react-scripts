import babelJest from "babel-jest"
import babelPreset from "../../babel-preset-react-app"

const hasJsxRuntime = (() => {
  if (process.env.DISABLE_NEW_JSX_TRANSFORM === "true") {
    return false
  }

  try {
    require.resolve("react/jsx-runtime")
    return true
  } catch (e) {
    return false
  }
})()

// https://github.com/facebook/jest/pull/11102
module.exports = babelJest.createTransformer({
  presets: [[babelPreset, { runtime: hasJsxRuntime ? "automatic" : "classic" }]],
  babelrc: false,
  configFile: false,
})

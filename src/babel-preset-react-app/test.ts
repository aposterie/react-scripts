/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import { create, Options } from "./create"

export default function test(_api: any, opts?: Options) {
  return create({ helpers: false, ...opts }, "test")
}

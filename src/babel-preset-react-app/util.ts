import { dirname } from "path"
import type { Options } from "./create"

export function validateBoolOption<T>(opt: T, name: keyof T, defaultValue?: boolean) {
  const value = opt[name] ?? defaultValue
  if (typeof value !== "boolean") {
    throw new Error(`Preset react-app: '${name}' option must be a boolean.`)
  }
  return value
}

export function getEnv(opts: Options, env: string) {
  const DEV = env === "development"
  const PROD = env === "production"
  const TEST = env === "test"

  const useAbsoluteRuntime = validateBoolOption(opts, "absoluteRuntime", true)

  let absoluteRuntimePath: string | undefined
  if (useAbsoluteRuntime) {
    absoluteRuntimePath = dirname(require.resolve("@babel/runtime/package.json"))
  }

  if (!DEV && !PROD && !TEST) {
    throw new Error(
      "Using `babel-preset-react-app` requires that you specify `NODE_ENV` or " +
        '`BABEL_ENV` environment variables. Valid values are "development", ' +
        '"test", and "production". Instead, received: ' +
        JSON.stringify(env) +
        "."
    )
  }

  return {
    DEV,
    PROD,
    TEST,
    absoluteRuntimePath,
  }
}

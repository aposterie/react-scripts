/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import { randomBytes } from "crypto"
import type { PartialConfig } from "@babel/core"

const macroCheck = /[./]macro/

export default () => ({
  // This function transforms the Babel configuration on a per-file basis
  config(config: PartialConfig, { source }: { source: string }) {
    // Babel Macros are notoriously hard to cache, so they shouldn't be
    // https://github.com/babel/babel/issues/8497
    // We naively detect macros using their package suffix and add a random token
    // to the caller, a valid option accepted by Babel, to compose a one-time
    // cacheIdentifier for the file. We cannot tune the loader options on a per
    // file basis.
    if (macroCheck.test(source)) {
      return {
        ...config.options,
        caller: {
          ...config.options.caller,
          craInvalidationToken: randomBytes(32).toString("hex"),
        },
      }
    }
    return config.options
  },
})

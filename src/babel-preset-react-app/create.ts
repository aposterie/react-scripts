/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import type { TransformOptions } from "@babel/core"
import { validateBoolOption, getEnv } from "./util"

import presetEnv from "@babel/preset-env"
import presetReact from "@babel/preset-react"
import pluginTypeScript from "@babel/plugin-transform-typescript"

import proposalClassProperties from "@babel/plugin-proposal-class-properties"
import proposalNumericSeparator from "@babel/plugin-proposal-numeric-separator"
import proposalOptionalChaining from "@babel/plugin-proposal-optional-chaining"
import proposalNullishCoalescingOperator from "@babel/plugin-proposal-nullish-coalescing-operator"
import proposalLogicalAssignmentOperators from "@babel/plugin-proposal-logical-assignment-operators"
import proposalDecorators from "@babel/plugin-proposal-decorators"

import pluginRuntime from "@babel/plugin-transform-runtime"
import pluginMacro from "babel-plugin-macros"

export interface Options {
  typescript?: boolean
  helpers?: boolean
  absoluteRuntime?: boolean
  runtime?: "automatic"
}

export function create(opts: Options = {}, env: string): TransformOptions {
  const { DEV, PROD, TEST, absoluteRuntimePath } = getEnv(opts, env)

  const isTypeScriptEnabled = validateBoolOption(opts, "typescript", true)
  const areHelpersEnabled = validateBoolOption(opts, "helpers", true)

  return {
    presets: [
      TEST && [
        // ES features necessary for user's Node version
        presetEnv,
        {
          targets: {
            node: "current",
          },
        },
      ],
      (PROD || DEV) && [
        // Latest stable ECMAScript features
        presetEnv,
        {
          // Allow importing core-js in entrypoint and use browserlist to select polyfills
          useBuiltIns: false,
          // Do not transform modules to CJS
          modules: false,
          // Exclude transforms that make all code slower
          exclude: ["transform-typeof-symbol"],
        },
      ],
      [
        presetReact,
        {
          // Adds component stack to warning messages
          // Adds __self attribute to JSX which React will use for some warnings
          development: DEV || TEST,
          // Will use the native built-in instead of trying to polyfill
          // behavior for any plugins that require one.
          ...(opts.runtime !== "automatic" ? { useBuiltIns: true } : {}),
          runtime: opts.runtime || "classic",
        },
      ],
    ].filter(Boolean),
    plugins: [
      isTypeScriptEnabled && [
        pluginTypeScript,
        {
          allExtensions: true,
          allowDeclareFields: true,
          allowNamespaces: true,
          isTSX: true,
        },
      ],
      // Experimental macros support. Will be documented after it's had some time
      // in the wild.
      pluginMacro,
      // Turn on legacy decorators for TypeScript files
      isTypeScriptEnabled && [proposalDecorators, { legacy: true }],
      // class { handleClick = () => { } }
      // Enable loose mode to use assignment instead of defineProperty
      // See discussion in https://github.com/facebook/create-react-app/issues/4263
      [
        proposalClassProperties,
        {
          loose: true,
        },
      ],
      // Adds Numeric Separators
      proposalNumericSeparator,
      // Polyfills the runtime needed for async/await, generators, and friends
      // https://babeljs.io/docs/en/babel-plugin-transform-runtime
      [
        pluginRuntime,
        {
          corejs: false,
          helpers: areHelpersEnabled,
          // By default, babel assumes babel/runtime version 7.0.0-beta.0,
          // explicitly resolving to match the provided helper functions.
          // https://github.com/babel/babel/issues/10261
          version: require("@babel/runtime/package.json").version,
          regenerator: false,
          // https://babeljs.io/docs/en/babel-plugin-transform-runtime#useesmodules
          // We should turn this on once the lowest version of Node LTS
          // supports ES Modules.
          useESModules: true,
          // Lets us encapsulate our runtime, ensuring the correct version
          // is used
          absoluteRuntime: absoluteRuntimePath,
        },
      ],
      // Optional chaining and nullish coalescing are supported in @babel/preset-env,
      // but not yet supported in webpack due to support missing from acorn.
      // These can be removed once webpack has support.
      // See https://github.com/facebook/create-react-app/issues/8445#issuecomment-588512250
      proposalOptionalChaining,
      proposalNullishCoalescingOperator,
      proposalLogicalAssignmentOperators,
    ].filter(Boolean),
  }
}

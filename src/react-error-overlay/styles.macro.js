const { createMacro } = require('babel-plugin-macros')

module.exports = createMacro(({ references, babel: { types: t } }) => {
  references.default.forEach(({ parentPath, parent }) => {
    parentPath.replaceWith(t.stringLiteral('var(--' + parent.property.name + ')'))
  })
})

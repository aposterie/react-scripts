/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
/// <reference path="../react-app.d.ts" />
import React from "react"
import * as ReactDOM from "react-dom"
import { unmountComponentAtNode } from "react-dom"
import { CompileErrorContainer } from "./containers/CompileErrorContainer"
import { RuntimeErrorContainer } from "./containers/RuntimeErrorContainer"
import * as cls from "./styles.module.scss"

let iframeRoot: HTMLDivElement | null = null

const ThemeProvider: React.FunctionComponent = ({ children }) => (
  <div className={cls.provider}>{children}</div>
)

function render({
  currentBuildError,
  currentRuntimeErrorRecords,
  dismissRuntimeErrors,
  editorHandler,
}) {
  if (currentBuildError) {
    return (
      <ThemeProvider>
        <CompileErrorContainer error={currentBuildError} editorHandler={editorHandler} />
      </ThemeProvider>
    )
  }
  if (currentRuntimeErrorRecords.length > 0) {
    return (
      <ThemeProvider>
        <RuntimeErrorContainer
          errorRecords={currentRuntimeErrorRecords}
          close={dismissRuntimeErrors}
          editorHandler={editorHandler}
        />
      </ThemeProvider>
    )
  }
  return null
}

declare global {
  interface Window {
    updateContent(errorOverlayProps: any): boolean
    __REACT_ERROR_OVERLAY_GLOBAL_HOOK__: {
      iframeReady(): void
    }
  }
}

window.updateContent = function updateContent(errorOverlayProps) {
  const renderedElement = render(errorOverlayProps)

  if (renderedElement === null) {
    unmountComponentAtNode(iframeRoot!)
    return false
  }

  // Update the overlay
  ReactDOM.render(renderedElement, iframeRoot)
  return true
}

document.body.style.margin = "0"
// Keep popup within body boundaries for iOS Safari
document.body.style["max-width"] = "100vw"
iframeRoot = document.createElement("div")
iframeRoot.className = cls.overlay
document.body.appendChild(iframeRoot)
window.parent.__REACT_ERROR_OVERLAY_GLOBAL_HOOK__.iframeReady()

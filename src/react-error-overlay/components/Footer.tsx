/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import React from "react"
import vars from "../styles.macro"
import styled from "@emotion/styled"

const Footer = (props: { line1: string; line2?: string }) => (
  <div>
    {props.line1}
    <br />
    {props.line2}
  </div>
)

export default styled(Footer)`
  font-family: system-ui, sans-serif;
  color: ${vars.footer};
  margin-top: 0.5rem;
  flex: 0 0 auto;
`

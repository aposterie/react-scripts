/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import React, { useState } from "react"
import vars from "../styles.macro"
import styled from "@emotion/styled"

const CollapseButton = styled.button`
  cursor: pointer;
  border: none;
  display: block;
  width: 100%;
  text-align: left;
  font-family: Consolas, Menlo, monospace;
  font-size: 1em;
  padding: 0px;
  line-height: 1.5;
  color: ${vars.color};
  background: ${vars.background};
`

type CollapsiblePropsType = {
  children: React.ReactNode[]
}

const Collapsible = (props: CollapsiblePropsType) => {
  const [collapsed, setCollapsed] = useState(true)

  const toggleCollapsed = () => {
    setCollapsed(!collapsed)
  }

  const count = props.children.length
  return (
    <div>
      <CollapseButton
        onClick={toggleCollapsed}
        style={{ marginBottom: collapsed ? "1.5em" : "0.6em" }}
      >
        {(collapsed ? "▶" : "▼") +
          ` ${count} stack frames were ` +
          (collapsed ? "collapsed." : "expanded.")}
      </CollapseButton>
      <div style={{ display: collapsed ? "none" : "block" }}>
        {props.children}
        <CollapseButton onClick={toggleCollapsed} style={{ marginBottom: "0.6em" }}>
          {`▲ ${count} stack frames were expanded.`}
        </CollapseButton>
      </div>
    </div>
  )
}

export default Collapsible

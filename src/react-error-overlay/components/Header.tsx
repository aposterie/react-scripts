/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from "react"
import vars from "../styles.macro"
import styled from "@emotion/styled"

const Container = styled.div`
  font-size: 2em;
  font-family: sans-serif;
  color: ${vars.headerColor};
  white-space: pre-wrap;
  /* Top bottom margin spaces header */
  /* Right margin revents overlap with close button */
  margin: 0 2rem 0.75rem 0;
  flex: 0 0 auto;
  max-height: 50%;
  overflow: auto;
`
export const Header = (props: { headerText: string }) => (
  <Container>{props.headerText}</Container>
)

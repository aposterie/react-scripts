/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React, { useEffect } from "react"

import styled from "@emotion/styled"
import vars from "../styles.macro"

const Overlay = styled.div`
  position: relative;
  display: inline-flex;
  flex-direction: column;
  height: 100%;
  width: 1024px;
  max-width: 100%;
  overflow-x: hidden;
  overflow-y: auto;
  padding: 0.5rem;
  box-sizing: border-box;
  text-align: left;
  font-family: Consolas, Menlo, monospace;
  font-size: 11px;
  white-space: pre-wrap;
  word-break: break-word;
  line-height: 1.5;
  color: ${vars.color};
`

type ErrorOverlayPropsType = {
  children: React.ReactNode
  shortcutHandler?: (eventKey: string) => void
}

let iframeWindow: Window | null = null

const getIframeWindow = (element: HTMLDivElement | null) => {
  if (element) {
    iframeWindow = element.ownerDocument.defaultView
  }
}

export function ErrorOverlay(props: ErrorOverlayPropsType) {
  const { shortcutHandler, children } = props

  useEffect(() => {
    const onKeyDown = (e: KeyboardEvent) => {
      shortcutHandler?.(e.key)
    }
    window.addEventListener("keydown", onKeyDown)
    iframeWindow?.addEventListener("keydown", onKeyDown)
    return () => {
      window.removeEventListener("keydown", onKeyDown)
      iframeWindow?.removeEventListener("keydown", onKeyDown)
    }
  }, [shortcutHandler])

  return <Overlay ref={getIframeWindow}>{children}</Overlay>
}

/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React, { CSSProperties } from "react"
import styled from "@emotion/styled"
import vars from "../styles.macro"

const Main = styled.div`
  margin-bottom: 0.5rem;
`

const ButtonContainer = styled.span`
  margin-right: 1em;
`

const NavButton = styled.button`
  border: none;
  border-radius: 4px;
  padding: 3px 6px;
  cursor: pointer;
  background-color: ${vars.navBackground};
  color: ${vars.navArrow};
`

const LeftButton: CSSProperties = {
  borderTopRightRadius: "0px",
  borderBottomRightRadius: "0px",
  marginRight: "1px",
}

const Right: CSSProperties = {
  borderTopLeftRadius: "0px",
  borderBottomLeftRadius: "0px",
}

type Callback = () => void

type NavigationBarPropsType = {
  currentError: number
  totalErrors: number
  previous: Callback
  next: Callback
}

function NavigationBar(props: NavigationBarPropsType) {
  const { currentError, totalErrors, previous, next } = props
  return (
    <Main>
      <ButtonContainer>
        <NavButton onClick={previous} style={LeftButton}>
          ←
        </NavButton>
        <NavButton onClick={next} style={Right}>
          →
        </NavButton>
      </ButtonContainer>
      {`${currentError} of ${totalErrors} errors on the page`}
    </Main>
  )
}

export default NavigationBar

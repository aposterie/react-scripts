/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import React from "react"
import vars from "../styles.macro"
import styled from "@emotion/styled"

const Container = styled.span`
  color: ${vars.closeColor};
  line-height: 1rem;
  font-size: 1.5rem;
  padding: 1rem;
  cursor: pointer;
  position: absolute;
  right: 0;
  top: 0;
`

export const CloseButton = ({ close }: { close(): void }) => (
  <Container title="Click or press Escape to dismiss." onClick={close}>
    ×
  </Container>
)

/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React from "react"
import { css } from "@emotion/css"
import vars from "../styles.macro"
import styled from "@emotion/styled"

const Pre = styled.pre`
  position: relative;
  display: block;
  padding: 0.5em;
  margin-top: 0.5em;
  margin-bottom: 0.5em;
  overflow-x: auto;
  white-space: pre-wrap;
  border-radius: 0.25rem;
`

const primaryPreStyle = css`
  background-color: ${vars.primaryPreBackground};
  color: ${vars.primaryPreColor};
`
const secondaryPreStyle = css`
  background-color: ${vars.secondaryPreBackground};
  color: ${vars.secondaryPreColor};
`

type CodeBlockPropsType = {
  main: boolean
  codeHTML: string
}

export const CodeBlock = ({ main, codeHTML }: CodeBlockPropsType) => (
  <Pre className={main ? primaryPreStyle : secondaryPreStyle}>
    <code
      className={css`
        font-family: Consolas, Menlo, monospace;
      `}
      dangerouslySetInnerHTML={{ __html: codeHTML }}
    />
  </Pre>
)

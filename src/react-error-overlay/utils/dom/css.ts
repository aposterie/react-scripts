/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import { CSSProperties } from "react"

export function applyStyles(element: HTMLElement, styles: CSSProperties) {
  element.setAttribute("style", "")
  Object.assign(element.style, styles)
}

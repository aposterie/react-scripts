/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

/** A container holding a script line. */
class ScriptLine {
  constructor(
    /** The line number of this line of source. */
    public lineNumber: number,
    /** The content (or value) of this line of source. */
    public content: string,
    /** Whether or not this line should be highlighted. Particularly useful for error reporting with context. */
    public highlight: boolean = false
  ) {}
}

/**
 * A representation of a stack frame.
 */
class StackFrame {
  functionName?: string

  _originalFunctionName?: string
  _originalFileName?: string
  _originalLineNumber?: number
  _originalColumnNumber?: number
  _originalScriptCode?: ScriptLine[]

  constructor(
    functionName?: string,
    public fileName?: string,
    public lineNumber?: number,
    public columnNumber?: number,
    public _scriptCode?: ScriptLine[],
    sourceFunctionName?: string,
    sourceFileName?: string,
    sourceLineNumber?: number,
    sourceColumnNumber?: number,
    sourceScriptCode?: ScriptLine[]
  ) {
    if (functionName?.startsWith("Object.")) {
      functionName = functionName.slice("Object.".length)
    }
    if (
      // Chrome has a bug with inferring function.name:
      // https://github.com/facebook/create-react-app/issues/2097
      // Let's ignore a meaningless name we get for top-level modules.
      functionName === "friendlySyntaxErrorLabel" ||
      functionName === "exports.__esModule" ||
      functionName === "<anonymous>" ||
      !functionName
    ) {
      functionName = undefined
    }
    this.functionName = functionName

    this._originalFunctionName = sourceFunctionName
    this._originalFileName = sourceFileName
    this._originalLineNumber = sourceLineNumber
    this._originalColumnNumber = sourceColumnNumber

    this._originalScriptCode = sourceScriptCode
  }

  /**
   * Returns the name of this function.
   */
  getFunctionName(): string {
    return this.functionName || "(anonymous function)"
  }

  /**
   * Returns the source of the frame.
   * This contains the file name, line number, and column number when available.
   */
  getSource(): string {
    let str = ""
    if (this.fileName != null) {
      str += this.fileName + ":"
    }
    if (this.lineNumber != null) {
      str += this.lineNumber + ":"
    }
    if (this.columnNumber != null) {
      str += this.columnNumber + ":"
    }
    return str.slice(0, -1)
  }

  /**
   * Returns a pretty version of this stack frame.
   */
  toString(): string {
    const functionName = this.getFunctionName()
    const source = this.getSource()
    return `${functionName}${source ? ` (${source})` : ``}`
  }
}

export { StackFrame, ScriptLine }
export default StackFrame

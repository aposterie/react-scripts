/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import StackFrame from "./stack-frame"
import { getSourceMap, SourceMap } from "./getSourceMap"
import { getLinesAround } from "./getLinesAround"

/**
 * Enhances a set of `StackFrame`s with their original positions and code (when available).
 * @param frames A set of `StackFrame`s which contain (generated) code positions.
 * @param [contextLines=3] The number of lines to provide before and after the line specified in the `StackFrame`.
 */
export async function map(frames: StackFrame[], contextLines = 3): Promise<StackFrame[]> {
  const cache = new Map<string, { fileSource: string; map: SourceMap }>()
  const files: string[] = frames
    .map(f => f.fileName)
    .filter(Boolean)
    .filter(fileName => !files.includes(fileName))

  await Promise.allSettled(
    files.map(async fileName => {
      const fetchUrl = fileName.startsWith("webpack-internal:")
        ? `/__get-internal-source?fileName=${encodeURIComponent(fileName)}`
        : fileName

      const fileSource = await fetch(fetchUrl).then(r => r.text())
      const map = await getSourceMap(fileName, fileSource)
      cache.set(fileName, { fileSource, map })
    })
  )

  return frames.map(frame => {
    const { functionName, fileName, lineNumber, columnNumber } = frame
    const { map, fileSource } = cache.get(fileName!) ?? {}
    if (map == null || lineNumber == null) {
      return frame
    }
    const { source, line, column } = map.getOriginalPosition(lineNumber, columnNumber!)
    const originalSource = source == null ? [] : map.getSource(source)
    return new StackFrame(
      functionName,
      fileName,
      lineNumber,
      columnNumber,
      getLinesAround(lineNumber, contextLines, fileSource!),
      functionName,
      source,
      line,
      column,
      getLinesAround(line, contextLines, originalSource)
    )
  })
}

/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import Anser from "anser"
import { AllHtmlEntities as Entities } from "html-entities"
import _var from "../styles.macro"

const entities = new Entities()

// Map ANSI colors from what babel-code-frame uses to base16-github
// See: https://github.com/babel/babel/blob/e86f62b304d280d0bab52c38d61842b853848ba6/packages/babel-code-frame/src/index.js#L9-L22
const colors = {
  reset: [_var.base05, "transparent"],
  black: _var.base05,
  red: _var.base08 /* marker, bg-invalid */,
  green: _var.base0B /* string */,
  yellow: _var.base08 /* capitalized, jsx_tag, punctuator */,
  blue: _var.base0C,
  magenta: _var.base0C /* regex */,
  cyan: _var.base0E /* keyword */,
  gray: _var.base03 /* comment, gutter */,
  lightgrey: _var.base01,
  darkgrey: _var.base03,
}

const anserMap = {
  "ansi-bright-black": "black",
  "ansi-bright-yellow": "yellow",
  "ansi-yellow": "yellow",
  "ansi-bright-green": "green",
  "ansi-green": "green",
  "ansi-bright-cyan": "cyan",
  "ansi-cyan": "cyan",
  "ansi-bright-red": "red",
  "ansi-red": "red",
  "ansi-bright-magenta": "magenta",
  "ansi-magenta": "magenta",
  "ansi-white": "darkgrey",
}

export function generateAnsiHTML(txt: string): string {
  const arr = new Anser().ansiToJson(entities.encode(txt), { use_classes: true })

  let result = ""
  let open = false

  for (const { content, fg } of arr) {
    content
      .split("\n")
      .map(part => part.replace("\r", ""))
      .forEach((part, i, { length }) => {
        if (!open) {
          result += '<span data-ansi-line="true">'
          open = true
        }
        const color = colors[anserMap[fg]]
        if (color != null) {
          result += `<span style="color: ${color};">${part}</span>`
        } else {
          if (fg != null) {
            console.log("Missing color mapping: ", fg)
          }
          result += `<span>${part}</span>`
        }
        if (i < length - 1) {
          result += "</span>"
          open = false
          result += "<br/>"
        }
      })
  }

  if (open) {
    result += "</span>"
    open = false
  }

  return result
}

/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import type { StackFrame } from "./stack-frame"
import { parse } from "./parser"
import { map } from "./mapper"
import { unmap } from "./unmapper"

export async function getStackFrames(
  error: Error & { __unmap_source?: string },
  unhandledRejection = false,
  contextSize = 3
): Promise<StackFrame[] | null> {
  const parsedFrames = parse(error)

  const enhancedFrames = error.__unmap_source
    ? await unmap(error.__unmap_source, parsedFrames, contextSize)
    : await map(parsedFrames, contextSize)

  if (
    enhancedFrames
      .map(f => f._originalFileName)
      .findIndex(f => f && !f.includes("node_modules")) === -1
  ) {
    return null
  }

  return enhancedFrames.filter(
    ({ functionName }) =>
      functionName == null ||
      !functionName.includes("__stack_frame_overlay_proxy_console__")
  )
}

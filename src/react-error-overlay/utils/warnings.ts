/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import type { ReactFrame } from "../effects/proxyConsole"

function stripInlineStacktrace(message: string): string {
  return message
    .split("\n")
    .filter(line => !line.match(/^\s*in/))
    .join("\n") // "  in Foo"
}

function message(
  warning: string,
  frames: ReactFrame[]
): { message: string; stack: string } {
  const message = stripInlineStacktrace(warning)

  // Reassemble the stack with full filenames provided by React
  let stack = ""
  let lastFilename: string | undefined
  let lastLineNumber: number | undefined
  for (const { fileName, lineNumber, name = "(anonymous function)" } of frames) {
    if (fileName == null || lineNumber == null) {
      continue
    }

    // TODO: instead, collapse them in the UI
    if (
      fileName === lastFilename &&
      typeof lineNumber === "number" &&
      typeof lastLineNumber === "number" &&
      Math.abs(lineNumber - lastLineNumber) < 3
    ) {
      continue
    }
    lastFilename = fileName
    lastLineNumber = lineNumber

    stack += `in ${name} (at ${fileName}:${lineNumber})\n`
  }

  return { message, stack }
}

export { message as massage }

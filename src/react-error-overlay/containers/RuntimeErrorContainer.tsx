/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React, { useState, FC } from "react"
import { ErrorOverlay } from "../components/ErrorOverlay"
import { CloseButton } from "../components/CloseButton"
import NavigationBar from "../components/NavigationBar"
import { RuntimeError } from "./RuntimeError"
import Footer from "../components/Footer"

import type { ErrorRecord } from "./RuntimeError"
import type { ErrorLocation } from "../utils/parseCompileError"

type Props = {
  errorRecords: ErrorRecord[]
  close(): void
  editorHandler(errorLoc: ErrorLocation): void
}

export const RuntimeErrorContainer: FC<Props> = props => {
  const { errorRecords, close } = props

  const [currentIndex, setCurrentIndex] = useState(0)

  const previous =
    currentIndex > 0
      ? () => setCurrentIndex(currentIndex - 1)
      : () => setCurrentIndex(errorRecords.length - 1)

  const next =
    currentIndex < errorRecords.length - 1
      ? () => setCurrentIndex(currentIndex + 1)
      : () => setCurrentIndex(0)

  const shortcutHandler = (key: string) => {
    if (key === "Escape") {
      close()
    } else if (key === "ArrowLeft") {
      previous()
    } else if (key === "ArrowRight") {
      next()
    }
  }

  const totalErrors = errorRecords.length
  return (
    <ErrorOverlay shortcutHandler={shortcutHandler}>
      <CloseButton close={close} />
      {totalErrors > 1 && (
        <NavigationBar
          currentError={currentIndex + 1}
          totalErrors={totalErrors}
          previous={previous}
          next={next}
        />
      )}
      <RuntimeError
        errorRecord={errorRecords[currentIndex]}
        editorHandler={props.editorHandler}
      />
      <Footer
        line1="This screen is visible only in development. It will not appear if the app crashes in production."
        line2="Open your browser’s developer console to further inspect this error.  Click the 'X' or hit ESC to dismiss this message."
      />
    </ErrorOverlay>
  )
}

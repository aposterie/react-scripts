/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import React, { FC } from "react"
import { ErrorOverlay } from "../components/ErrorOverlay"
import Footer from "../components/Footer"
import { Header } from "../components/Header"
import { CodeBlock } from "../components/CodeBlock"
import { generateAnsiHTML } from "../utils/generateAnsiHTML"
import { parseCompileError } from "../utils/parseCompileError"
import type { ErrorLocation } from "../utils/parseCompileError"

type CompileErrorContainerPropsType = {
  error: string
  editorHandler(errorLoc: ErrorLocation): void
}

export const CompileErrorContainer: FC<CompileErrorContainerPropsType> = ({
  error,
  editorHandler,
}) => {
  const errLoc = parseCompileError(error)
  const canOpenInEditor = errLoc !== null && editorHandler !== null
  return (
    <ErrorOverlay>
      <Header headerText="Failed to compile" />
      <div
        onClick={canOpenInEditor && errLoc ? () => editorHandler(errLoc) : undefined}
        style={canOpenInEditor ? { cursor: "pointer" } : undefined}
      >
        <CodeBlock main={true} codeHTML={generateAnsiHTML(error)} />
      </div>
      <Footer line1="This error occurred during the build time and cannot be dismissed." />
    </ErrorOverlay>
  )
}

/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React, { useState } from "react"
import CodeBlock, { StackFrameCodeBlockPropsType } from "./StackFrameCodeBlock"
import { getPrettyURL } from "../utils/getPrettyURL"

import type { StackFrame as StackFrameType } from "../utils/stack-frame"
import type { ErrorLocation } from "../utils/parseCompileError"
import styled from "@emotion/styled"
import { css } from "@emotion/css"
import vars from "../styles.macro"

const LinkStyle = styled.div`
  font-size: 0.9em;
  margin-bottom: 0.9em;
`

const anchorStyle = css`
  text-decoration: none;
  color: ${vars.anchorColor};
  cursor: pointer;
`

const codeAnchorStyle = css`
  cursor: pointer;
`

const ToggleStyle = styled.button`
  margin-bottom: 1.5em;
  color: ${vars.toggleColor};
  cursor: pointer;
  border: none;
  display: block;
  width: 100%;
  text-align: left;
  background: ${vars.toggleBackground};
  font-family: Consolas, Menlo, monospace;
  font-size: 1em;
  padding: 0px;
  line-height: 1.5;
`

type StackFramePropsType = {
  frame: StackFrameType
  contextSize: number
  critical: boolean
  showCode: boolean
  editorHandler: (errorLoc: ErrorLocation) => void
}

export function StackFrame(props: StackFramePropsType) {
  const [compiled, setCompiled] = useState(false)

  const toggleCompiled = () => {
    setCompiled(!compiled)
  }

  const getErrorLocation = (): ErrorLocation | null => {
    const { _originalFileName: fileName, _originalLineNumber: lineNumber } = props.frame
    // Unknown file
    if (!fileName) {
      return null
    }
    // e.g. "/path-to-my-app/webpack/bootstrap eaddeb46b67d75e4dfc1"
    const isInternalWebpackBootstrapCode = fileName.trim().indexOf(" ") !== -1
    if (isInternalWebpackBootstrapCode) {
      return null
    }
    // Code is in a real file
    return { fileName, lineNumber: lineNumber || 1 }
  }

  const editorHandler = () => {
    const errorLoc = getErrorLocation()
    if (!errorLoc) {
      return
    }
    props.editorHandler(errorLoc)
  }

  const onKeyDown = (e: React.KeyboardEvent<any>) => {
    if (e.key === "Enter") {
      editorHandler()
    }
  }

  const { frame, contextSize, critical, showCode } = props
  const {
    fileName,
    lineNumber,
    columnNumber,
    _scriptCode: scriptLines,
    _originalFileName: sourceFileName,
    _originalLineNumber: sourceLineNumber,
    _originalColumnNumber: sourceColumnNumber,
    _originalScriptCode: sourceLines,
  } = frame
  const functionName = frame.getFunctionName()

  const url = getPrettyURL(
    sourceFileName,
    sourceLineNumber,
    sourceColumnNumber,
    fileName,
    lineNumber,
    columnNumber,
    compiled
  )

  let codeBlockProps: StackFrameCodeBlockPropsType | undefined
  if (showCode) {
    if (compiled && scriptLines?.length && lineNumber != null) {
      codeBlockProps = {
        lines: scriptLines,
        lineNum: lineNumber,
        columnNum: columnNumber,
        contextSize,
        main: critical,
      }
    } else if (!compiled && sourceLines?.length && sourceLineNumber != null) {
      codeBlockProps = {
        lines: sourceLines,
        lineNum: sourceLineNumber,
        columnNum: sourceColumnNumber,
        contextSize,
        main: critical,
      }
    }
  }

  const canOpenInEditor = getErrorLocation() !== null && props.editorHandler !== null
  return (
    <div>
      <div>{functionName}</div>
      <LinkStyle>
        <span
          className={canOpenInEditor ? anchorStyle : undefined}
          onClick={canOpenInEditor ? editorHandler : undefined}
          onKeyDown={canOpenInEditor ? onKeyDown : undefined}
          tabIndex={canOpenInEditor ? 0 : undefined}
        >
          {url}
        </span>
      </LinkStyle>
      {codeBlockProps && (
        <span>
          <span
            onClick={canOpenInEditor ? editorHandler : undefined}
            className={canOpenInEditor ? codeAnchorStyle : undefined}
          >
            <CodeBlock {...codeBlockProps} />
          </span>
          <ToggleStyle onClick={toggleCompiled}>
            {"View " + (compiled ? "source" : "compiled")}
          </ToggleStyle>
        </span>
      )}
    </div>
  )
}

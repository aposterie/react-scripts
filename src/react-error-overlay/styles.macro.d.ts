import { Theme } from "./styles"

declare const _var: { [key in keyof Theme]: key }
export default _var

// Do this as the first thing so that any code reading it knows the right env.
import "./utils/env-prod"

// Ensure environment variables are read.
import "../config/env"
// @remove-on-eject-begin
import verifyTypeScriptSetup from "./utils/verifyTypeScriptSetup"
verifyTypeScriptSetup()
// @remove-on-eject-end

import { relative } from "path"
import chalk, { yellow, green, red } from "../react-dev-utils/chalk"
import { existsSync, emptyDirSync, copySync } from "fs-extra"
import { write } from "bfj"
import webpack from "webpack"
import configFactory from "../config/webpack.config"
import createPaths from "../config/paths"
import checkRequiredFiles from "../react-dev-utils/checkRequiredFiles"
import formatWebpackMessages, {
  FormattedWebpackMessages,
} from "../react-dev-utils/formatWebpackMessages"
import printHostingInstructions from "../react-dev-utils/printHostingInstructions"
import {
  measureFileSizesBeforeBuild,
  printFileSizesAfterBuild,
} from "../react-dev-utils/FileSizeReporter"
import printBuildError from "../react-dev-utils/printBuildError"

const paths = createPaths()

const useYarn = existsSync(paths.yarnLockFile)

// These sizes are pretty large. We'll warn for bundles exceeding them.
const WARN_AFTER_BUNDLE_GZIP_SIZE = 512 * 1024
const WARN_AFTER_CHUNK_GZIP_SIZE = 1024 * 1024

const isInteractive = process.stdout.isTTY

// Warn and crash if required files are missing
if (!checkRequiredFiles([paths.appHtml, paths.appIndexJs])) {
  process.exit(1)
}

const argv = process.argv.slice(2)
const writeStatsJson = argv.indexOf("--stats") !== -1

// We require that you explicitly set browsers and do not fall back to
// browserslist defaults.
import { checkBrowsers } from "../react-dev-utils/browsersHelper"

// Generate configuration
const config = configFactory("production")

//
;(async function main() {
  try {
    await checkBrowsers(paths.appPath, isInteractive)
    // First, read the current file sizes in build directory.
    // This lets us display how much they changed later.
    const previousFileSizes = await measureFileSizesBeforeBuild(paths.appBuild)

    // Remove all content but keep the directory so that
    // if you're in it, you don't end up in Trash
    emptyDirSync(paths.appBuild)
    // Merge with the public folder
    copyPublicFolder()

    try {
      // Start the webpack build
      const { stats, warnings } = await build()

      if (warnings.length) {
        console.log(
          chalk`
            {yellow Compiled with warnings.}

            ${warnings.join("\n\n")}

            Search for the {underline.yellow keywords} to learn more about each warning.
            To ignore, add {cyan // eslint-disable-next-line} to the line before.

          `
        )
      } else {
        console.log(green("Compiled successfully.\n"))
      }

      console.log("File sizes after gzip:\n")
      printFileSizesAfterBuild(
        stats,
        previousFileSizes,
        paths.appBuild,
        WARN_AFTER_BUNDLE_GZIP_SIZE,
        WARN_AFTER_CHUNK_GZIP_SIZE
      )
      console.log()

      const appPackage = require(paths.appPackageJson)
      const publicUrl = paths.publicUrlOrPath
      const publicPath = (await config).output!.publicPath!
      const buildFolder = relative(process.cwd(), paths.appBuild)
      printHostingInstructions(appPackage, publicUrl, publicPath, buildFolder, useYarn)
    } catch (err) {
      const tscCompileOnError = process.env.TSC_COMPILE_ON_ERROR === "true"
      if (tscCompileOnError) {
        console.warn(
          yellow(
            "Compiled with the following type errors (you may want to check these before deploying your app):\n"
          )
        )
        printBuildError(err)
      } else {
        console.error(red("Failed to compile.\n"))
        printBuildError(err)
        process.exit(1)
      }
    }
  } catch (err) {
    if (err?.message) {
      console.error(err.message)
    }
    process.exit(1)
  }
})()

// Create the production build and print the deployment instructions.
async function build() {
  console.log("Creating an optimized production build...")

  const compiler = webpack(await config)

  return new Promise<{
    stats: webpack.Stats
    warnings: string[]
  }>((resolve, reject) => {
    compiler.run((err, stats) => {
      let messages: FormattedWebpackMessages
      if (err) {
        if (!err.message) {
          return reject(err)
        }

        let errMessage = err.message

        // Add additional information for postcss errors
        if ("postcssNode" in err) {
          errMessage += `\nCompileError: Begins at CSS selector ${
            (err as any).postcssNode.selector
          }`
        }

        messages = formatWebpackMessages({
          errors: [errMessage],
          warnings: [],
        })
      } else {
        messages = formatWebpackMessages(
          stats.toJson({ all: false, warnings: true, errors: true })
        )
      }
      if (messages.errors.length) {
        // Only keep the first error. Others are often indicative
        // of the same problem, but confuse the reader with noise.
        if (messages.errors.length > 1) {
          messages.errors = [messages.errors[0]]
        }
        return reject(new Error(messages.errors.join("\n\n")))
      }
      if (
        process.env.CI &&
        (typeof process.env.CI !== "string" ||
          process.env.CI.toLowerCase() !== "false") &&
        messages.warnings.length
      ) {
        console.log(
          yellow(
            "\nTreating warnings as errors because process.env.CI = true.\n" +
              "Most CI servers set it automatically.\n"
          )
        )
        return reject(new Error(messages.warnings.join("\n\n")))
      }

      const resolveArgs = {
        stats,
        warnings: messages.warnings,
      }

      if (writeStatsJson) {
        return write(paths.appBuild + "/bundle-stats.json", stats.toJson())
          .then(() => resolve(resolveArgs))
          .catch(error => reject(new Error(error)))
      }

      return resolve(resolveArgs)
    })
  })
}

function copyPublicFolder() {
  copySync(paths.appPublic, paths.appBuild, {
    dereference: true,
    filter: file => file !== paths.appHtml,
  })
}

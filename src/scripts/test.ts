// Do this as the first thing so that any code reading it knows the right env.
import "./utils/env-test"

// Ensure environment variables are read.
import "../config/env"
// @remove-on-eject-begin
// Do the preflight check (only happens before eject).
import verifyTypeScriptSetup from "./utils/verifyTypeScriptSetup"
verifyTypeScriptSetup()
// @remove-on-eject-end

import * as jest from "jest"
import { execSync } from "child_process"
import path from "path"
import resolve from "resolve"

let argv = process.argv.slice(2)

function isInGitRepository() {
  try {
    execSync("git rev-parse --is-inside-work-tree", { stdio: "ignore" })
    return true
  } catch {
    return false
  }
}

function isInMercurialRepository() {
  try {
    execSync("hg --cwd . root", { stdio: "ignore" })
    return true
  } catch {
    return false
  }
}

// Watch unless on CI or explicitly running all tests
if (
  !process.env.CI &&
  !argv.includes("--watchAll") &&
  !argv.includes("--watchAll=false")
) {
  // https://github.com/facebook/create-react-app/issues/5210
  const hasSourceControl = isInGitRepository() || isInMercurialRepository()
  argv.push(hasSourceControl ? "--watch" : "--watchAll")
}

// @remove-on-eject-begin
// This is not necessary after eject because we embed config into package.json.
import createJestConfig from "./utils/createJestConfig"
import paths from "../config/paths"
argv.push(
  "--config",
  JSON.stringify(
    createJestConfig(
      relativePath => path.resolve(__dirname, "..", relativePath),
      path.resolve(paths().appSrc, "..")
    )
  )
)

// This is a very dirty workaround for https://github.com/facebook/jest/issues/5913.
// We're trying to resolve the environment ourselves because Jest does it incorrectly.
// TODO: remove this as soon as it's fixed in Jest.
function resolveJestDefaultEnvironment(name: string) {
  const jestDir = path.dirname(
    resolve.sync("jest", {
      basedir: __dirname,
    })
  )
  const jestCLIDir = path.dirname(
    resolve.sync("jest-cli", {
      basedir: jestDir,
    })
  )
  const jestConfigDir = path.dirname(
    resolve.sync("jest-config", {
      basedir: jestCLIDir,
    })
  )
  return resolve.sync(name, {
    basedir: jestConfigDir,
  })
}

const cleanArgv: string[] = []
let env = "jsdom"
let next: string
do {
  next = argv.shift()!
  if (next === "--env") {
    env = argv.shift()!
  } else if (next.startsWith("--env=")) {
    env = next.substring("--env=".length)
  } else {
    cleanArgv.push(next)
  }
} while (argv.length > 0)
argv = cleanArgv
let resolvedEnv: string | undefined
try {
  resolvedEnv = resolveJestDefaultEnvironment(`jest-environment-${env}`)
} catch {}
if (!resolvedEnv) {
  try {
    resolvedEnv = resolveJestDefaultEnvironment(env)
  } catch {}
}
const testEnvironment = resolvedEnv || env
argv.push("--env", testEnvironment)
// @remove-on-eject-end

// https://github.com/facebook/create-react-app/blob/master/packages/react-app-polyfill/jsdom.js
// fetch() polyfill for making API calls.
import "whatwg-fetch"

jest.run(argv)

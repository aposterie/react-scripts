// Do this as the first thing so that any code reading it knows the right env.
import "./utils/env-dev"

// Ensure environment variables are read.
import "../config/env"

import verifyTypeScriptSetup from "./utils/verifyTypeScriptSetup"
verifyTypeScriptSetup()

import { existsSync } from "fs"
import { cyan, yellow, bold } from "../react-dev-utils/chalk"
import webpack from "webpack"
import WebpackDevServer from "webpack-dev-server"
import clearConsole from "../react-dev-utils/clearConsole"
import checkRequiredFiles from "../react-dev-utils/checkRequiredFiles"
import {
  choosePort,
  createCompiler,
  prepareProxy,
  prepareUrls,
} from "../react-dev-utils/WebpackDevServerUtils"
import openBrowser from "../react-dev-utils/openBrowser"
import createPaths from "../config/paths"
import configFactory from "../config/webpack.config"
import createDevServerConfig from "../config/webpackDevServer.config"

const paths = createPaths()
const useYarn = existsSync(paths.yarnLockFile)
const isInteractive = process.stdout.isTTY

// Warn and crash if required files are missing
if (!checkRequiredFiles([paths.appHtml, paths.appIndexJs])) {
  process.exit(1)
}

// Tools like Cloud9 rely on this.
const DEFAULT_PORT = parseInt(process.env.PORT || "3000", 10)
const HOST = process.env.HOST || "0.0.0.0"

if (process.env.HOST) {
  console.log(
    cyan(
      `Attempting to bind to HOST environment variable: ${yellow(bold(process.env.HOST))}`
    )
  )
  console.log(
    `If this was unintentional, check that you haven't mistakenly set it in your shell.`
  )
  console.log(`Learn more here: ${yellow("https://cra.link/advanced-config")}`)
  console.log()
}

// We require that you explicitly set browsers and do not fall back to
// browserslist defaults.
import { checkBrowsers } from "../react-dev-utils/browsersHelper"

//
;(async function () {
  try {
    await checkBrowsers(paths.appPath, isInteractive)

    // We attempt to use the default port but if it is busy, we offer the user to
    // run on a different port. `choosePort()` Promise resolves to the next free port.
    const port = await choosePort(HOST, DEFAULT_PORT)

    if (port == null) {
      // We have not found a port.
      return
    }

    const config = await configFactory("development")
    const protocol = process.env.HTTPS === "true" ? "https" : "http"
    const appName = require(paths.appPackageJson).name
    const useTypeScript = existsSync(paths.appTsConfig)
    const tscCompileOnError = process.env.TSC_COMPILE_ON_ERROR === "true"
    const urls = prepareUrls(protocol, HOST, port, paths.publicUrlOrPath.slice(0, -1))

    // Create a webpack compiler that is configured with custom messages.
    const compiler = createCompiler({
      appName,
      config,
      devSocket: {
        warnings: warnings =>
          devServer.sockWrite(devServer.sockets, "warnings", warnings),
        errors: errors => devServer.sockWrite(devServer.sockets, "errors", errors),
      },
      urls,
      useYarn,
      useTypeScript,
      tscCompileOnError,
      webpack,
    })

    // Load proxy config
    const proxySetting = require(paths.appPackageJson).proxy
    const proxyConfig = prepareProxy(proxySetting, paths.appPublic, paths.publicUrlOrPath)
    // Serve webpack assets generated by the compiler over a web server.
    const serverConfig = createDevServerConfig(proxyConfig, urls.lanUrlForConfig)
    const devServer = new WebpackDevServer(compiler, serverConfig)
    // Launch WebpackDevServer.
    devServer.listen(port, HOST, err => {
      if (err) {
        return console.log(err)
      }
      if (isInteractive) {
        clearConsole()
      }

      console.log(cyan(`Starting the development server on http://${HOST}:${port} ...\n`))

      openBrowser(urls.localUrlForBrowser)
    })

    for (const sig of ["SIGINT", "SIGTERM"]) {
      process.on(sig, () => {
        devServer.close()
        process.exit()
      })
    }

    if (process.env.CI !== "true") {
      // Gracefully exit when stdin ends
      process.stdin.on("end", () => {
        devServer.close()
        process.exit()
      })
    }
  } catch (err) {
    console.trace()
    if (err?.message) {
      console.log(err.message)
    }
    process.exit(1)
  }
})()

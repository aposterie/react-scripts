/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import chalk, { yellow, bold, red, cyan } from "../../react-dev-utils/chalk"
import * as fs from "fs"
import { sync as resolve } from "resolve"
import * as path from "path"
import createPaths from "../../config/paths"
import { EOL } from "os"
import immer, { produce, setAutoFreeze } from "immer"
import globby from "globby"
import type { Tsconfig } from "./tsconfig"
import type { ParsedCommandLine, CompilerOptions } from "typescript"

const paths = createPaths()

setAutoFreeze(false)

function writeJson(fileName: string, object: any) {
  fs.writeFileSync(fileName, JSON.stringify(object, null, 2).replace(/\n/g, EOL) + EOL)
}

function verifyNoTypeScript() {
  const typescriptFiles = globby.sync(
    ["**/*.(ts|tsx)", "!**/node_modules", "!**/*.d.ts"],
    { cwd: paths.appSrc }
  )

  if (typescriptFiles.length > 0) {
    console.warn(
      yellow(
        `We detected TypeScript in your project (${bold(
          `src${path.sep}${typescriptFiles[0]}`
        )}) and created a ${bold("tsconfig.json")} file for you.`
      )
    )
    console.warn()
    return false
  }
  return true
}

export default function verifyTypeScriptSetup() {
  let firstTimeSetup = false

  if (!fs.existsSync(paths.appTsConfig)) {
    if (verifyNoTypeScript()) {
      return
    }
    writeJson(paths.appTsConfig, {})
    firstTimeSetup = true
  }

  const isYarn = fs.existsSync(paths.yarnLockFile)

  // Ensure typescript is installed
  let ts: typeof import("typescript")
  try {
    // TODO: Remove this hack once `globalThis` issue is resolved
    // https://github.com/jsdom/jsdom/issues/2961
    const globalThisWasDefined = !!global.globalThis

    ts = require(resolve("typescript", {
      basedir: paths.appNodeModules,
    }))

    if (!globalThisWasDefined && !!global.globalThis) {
      delete (global as any).globalThis
    }
  } catch {
    console.error(
      chalk.bold
        .red`looks like you're trying to use TypeScript but do not have {bold typescript} installed.`
    )
    console.error(
      bold(
        "Please install",
        cyan.bold("typescript"),
        "by running",
        cyan.bold(isYarn ? "yarn add typescript" : "npm install typescript") + "."
      )
    )
    console.error(
      chalk.bold`If you are not trying to use TypeScript, please remove the {cyan tsconfig.json} file from your package root (and any TypeScript files).`
    )
    console.error()
    process.exit(1)
  }

  const compilerOptions = new Map<
    string,
    {
      parsedValue?: number
      suggested?: any
      value?: any
      reason?: string
    }
  >(
    Object.entries({
      // These are suggested values and will be set when not present in the
      // tsconfig.json
      // 'parsedValue' matches the output value from ts.parseJsonConfigFileContent()
      target: {
        parsedValue: ts.ScriptTarget.ES2017,
        suggested: "es2017",
      },
      lib: { suggested: ["dom", "dom.iterable", "esnext"] },
      allowJs: { suggested: true },
      skipLibCheck: { suggested: true },
      esModuleInterop: { suggested: true },
      allowSyntheticDefaultImports: { suggested: true },
      strict: { suggested: true },
      forceConsistentCasingInFileNames: { suggested: true },
      noFallthroughCasesInSwitch: { suggested: true },

      // These values are required and cannot be changed by the user
      // Keep this in sync with the webpack config
      module: {
        parsedValue: ts.ModuleKind.ESNext,
        value: "esnext",
        reason: "for import() and import/export",
      },
      moduleResolution: {
        parsedValue: ts.ModuleResolutionKind.NodeJs,
        value: "node",
        reason: "to match webpack resolution",
      },
      resolveJsonModule: { value: true, reason: "to match webpack loader" },
      isolatedModules: { value: true, reason: "implementation limitation" },
      noEmit: { value: true },
      jsx: {
        parsedValue: ts.JsxEmit.React,
        suggested: "react",
      },
    })
  )

  const formatDiagnosticHost = {
    getCanonicalFileName: (fileName: string) => fileName,
    getCurrentDirectory: ts.sys.getCurrentDirectory,
    getNewLine: () => EOL,
  }

  const messages: string[] = []
  let appTsConfig: Tsconfig
  let parsedTsConfig: Tsconfig = {}
  let parsedCompilerOptions: CompilerOptions = {}
  try {
    const { config: readTsConfig, error } = ts.readConfigFile(
      paths.appTsConfig,
      ts.sys.readFile
    )

    if (error) {
      throw new Error(ts.formatDiagnostic(error, formatDiagnosticHost))
    }

    appTsConfig = readTsConfig

    // Get TS to parse and resolve any "extends"
    // Calling this function also mutates the tsconfig above,
    // adding in "include" and "exclude", but the compilerOptions remain untouched
    let result: Partial<ParsedCommandLine> = {}
    // eslint-disable-next-line @typescript-eslint/ban-types
    parsedTsConfig = produce(readTsConfig as {}, config => {
      result = ts.parseJsonConfigFileContent(
        config,
        ts.sys,
        path.dirname(paths.appTsConfig)
      )
    })

    if (result.errors && result.errors.length) {
      throw new Error(ts.formatDiagnostic(result.errors[0], formatDiagnosticHost))
    }

    parsedCompilerOptions = result.options ?? {}
  } catch (e) {
    if (e && e.name === "SyntaxError") {
      console.error(
        red.bold(
          "Could not parse",
          cyan("tsconfig.json") + ".",
          "Please make sure it contains syntactically correct JSON."
        )
      )
    }

    console.log(e && e.message ? `${e.message}` : "")
    process.exit(1)
  }

  if (appTsConfig.compilerOptions == null) {
    appTsConfig.compilerOptions = {}
    firstTimeSetup = true
  }

  for (const [option, { parsedValue, value, suggested, reason }] of compilerOptions) {
    const valueToCheck = parsedValue === undefined ? value : parsedValue
    const coloredOption = cyan(`compilerOptions.${option}`)

    if (suggested != null) {
      if (parsedCompilerOptions[option] === undefined) {
        appTsConfig = immer(appTsConfig, config => {
          config.compilerOptions![option] = suggested
        })
        messages.push(
          `${coloredOption} to be ${bold("suggested")} value: ${cyan.bold(
            suggested
          )} (this can be changed)`
        )
      }
    } else if (parsedCompilerOptions[option] !== valueToCheck) {
      appTsConfig = immer(appTsConfig, config => {
        config.compilerOptions![option] = value
      })
      messages.push(
        `${coloredOption} ${bold(valueToCheck == null ? "must not" : "must")} be ${
          valueToCheck == null ? "set" : cyan.bold(value)
        }` + (reason != null ? ` (${reason})` : "")
      )
    }
  }

  // tsconfig will have the merged "include" and "exclude" by this point
  if (parsedTsConfig.include == null) {
    appTsConfig = immer(appTsConfig, config => {
      config.include = ["src"]
    })
    messages.push(`${cyan("include")} should be ${cyan.bold("src")}`)
  }

  if (messages.length > 0) {
    if (firstTimeSetup) {
      console.log(bold`Your {cyan tsconfig.json} has been populated with default values.`)
      console.log()
    } else {
      console.warn(
        bold`The following changes are being made to your {cyan tsconfig.json} file:`
      )
      messages.forEach(message => {
        console.warn("  - " + message)
      })
      console.warn()
    }
    writeJson(paths.appTsConfig, appTsConfig)
  }

  // Reference `react-scripts` types
  if (!fs.existsSync(paths.appTypeDeclarations)) {
    fs.writeFileSync(
      paths.appTypeDeclarations,
      `/// <reference types="react-scripts" />${EOL}`
    )
  }
}

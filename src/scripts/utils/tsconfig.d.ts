import type { CompilerOptions, TypeAcquisition, ProjectReference } from "typescript"
import type { TsConfigOptions } from "ts-node"

export interface Tsconfig {
  /**
   * Instructs the TypeScript compiler how to compile .ts files.
   */
  compilerOptions?: CompilerOptions
  /**
   * Enable Compile-on-Save for this project.
   */
  compileOnSave?: boolean
  /**
   * Auto type (.d.ts) acquisition options for this project. Requires TypeScript version 2.1
   * or later.
   */
  typeAcquisition?: TypeAcquisition
  /**
   * Path to base configuration file to inherit from. Requires TypeScript version 2.1 or later.
   */
  extends?: string
  /**
   * ts-node options.  See also: https://github.com/TypeStrong/ts-node#configuration-options
   *
   * ts-node offers TypeScript execution and REPL for node.js, with source map support.
   */
  "ts-node"?: TsConfigOptions
  /**
   * If no 'files' or 'include' property is present in a tsconfig.json, the compiler defaults
   * to including all files in the containing directory and subdirectories except those
   * specified by 'exclude'. When a 'files' property is specified, only those files and those
   * specified by 'include' are included.
   */
  files?: string[]
  /**
   * Specifies a list of files to be excluded from compilation. The 'exclude' property only
   * affects the files included via the 'include' property and not the 'files' property. Glob
   * patterns require TypeScript version 2.0 or later.
   */
  exclude?: string[]
  /**
   * Specifies a list of glob patterns that match files to be included in compilation. If no
   * 'files' or 'include' property is present in a tsconfig.json, the compiler defaults to
   * including all files in the containing directory and subdirectories except those specified
   * by 'exclude'. Requires TypeScript version 2.0 or later.
   */
  include?: string[]
  /**
   * Referenced projects. Requires TypeScript version 3.0 or later.
   */
  references?: ProjectReference[]
}

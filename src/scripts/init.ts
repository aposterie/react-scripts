import {
  removeSync,
  existsSync,
  writeFileSync,
  renameSync,
  copySync,
  readFileSync,
  appendFileSync,
  unlinkSync,
  moveSync,
} from "fs-extra"
import { mapValues } from "lodash"
import { join, dirname } from "path"
import chalk, { yellow, green } from "../react-dev-utils/chalk"
import { execSync } from "child_process"
import { sync } from "cross-spawn"
import { defaultBrowsers } from "../react-dev-utils/browsersHelper"
import { EOL } from "os"
import verifyTypeScriptSetup from "./utils/verifyTypeScriptSetup"

const { log, warn, error } = console

function isInGitRepository() {
  try {
    execSync("git rev-parse --is-inside-work-tree", { stdio: "ignore" })
    return true
  } catch {
    return false
  }
}

function isInMercurialRepository() {
  try {
    execSync("hg --cwd . root", { stdio: "ignore" })
    return true
  } catch {
    return false
  }
}

function tryGitInit() {
  try {
    execSync("git --version", { stdio: "ignore" })
    if (isInGitRepository() || isInMercurialRepository()) {
      return false
    }

    execSync("git init", { stdio: "ignore" })
    return true
  } catch (e) {
    warn("Git repo not initialized", e)
    return false
  }
}

function tryGitCommit(appPath: string) {
  try {
    execSync("git add -A", { stdio: "ignore" })
    execSync('git commit -m "Initialize project using Create React App"', {
      stdio: "ignore",
    })
    return true
  } catch (e) {
    // We couldn't commit in already initialized git repo,
    // maybe the commit author config is not set.
    // In the future, we might supply our own committer
    // like Ember CLI does, but for now, let's just
    // remove the Git files to avoid a half-done state.
    warn("Git commit not created", e)
    warn("Removing .git directory...")
    try {
      // unlinkSync() doesn't work on directories.
      removeSync(join(appPath, ".git"))
    } catch (removeErr) {
      // Ignore.
    }
    return false
  }
}

export default function (
  appPath: string,
  appName: string,
  verbose: boolean,
  originalDirectory: string,
  templateName: string
) {
  const appPackage: {
    [key: string]: any
    dependencies: Record<string, string>
    scripts: Record<string, string>
    eslintConfig?: Record<string, string>
    browserslist?: Record<string, string[]>
  } = require(join(appPath, "package.json"))
  const useYarn = existsSync(join(appPath, "yarn.lock"))

  if (!templateName) {
    log("")
    error(
      chalk`A template was not provided. This is likely because you're using an outdated version of {cyan create-react-app}.`
    )
    error(
      chalk`Please note that global installs of {cyan create-react-app} are no longer supported.`
    )
    error(
      chalk`You can fix this by running {cyan npm uninstall -g create-react-app} or {cyan yarn global remove create-react-app} before using {cyan create-react-app} again.`
    )
    return
  }

  const templatePath = dirname(
    require.resolve(`${templateName}/package.json`, { paths: [appPath] })
  )

  const templateJsonPath = join(templatePath, "template.json")

  let templateJson: {
    dependencies?: Record<string, string>
    scripts?: Record<string, string>
    package?: Record<string, Record<string, string>>
  } = {}
  if (existsSync(templateJsonPath)) {
    templateJson = require(templateJsonPath)
  }

  const templatePackage = templateJson.package || {}

  // TODO: Deprecate support for root-level `dependencies` and `scripts` in v5.
  // These should now be set under the `package` key.
  if (templateJson.dependencies || templateJson.scripts) {
    log(
      chalk.yellow`

        Root-level \`dependencies\` and \`scripts\` keys in \`template.json\` are deprecated.
        This template should be updated to use the new \`package\` key.
      `
    )
    log("For more information, visit https://cra.link/templates")
  }
  if (templateJson.dependencies) {
    templatePackage.dependencies = templateJson.dependencies
  }
  if (templateJson.scripts) {
    templatePackage.scripts = templateJson.scripts
  }

  // Keys to ignore in templatePackage
  const templatePackageBlacklist = [
    "name",
    "version",
    "description",
    "keywords",
    "bugs",
    "license",
    "author",
    "contributors",
    "files",
    "browser",
    "bin",
    "man",
    "directories",
    "repository",
    "peerDependencies",
    "bundledDependencies",
    "optionalDependencies",
    "engineStrict",
    "os",
    "cpu",
    "preferGlobal",
    "private",
    "publishConfig",
  ]

  // Keys from templatePackage that will be merged with appPackage
  const templatePackageToMerge = ["dependencies", "scripts"]

  // Keys from templatePackage that will be added to appPackage,
  // replacing any existing entries.
  const templatePackageToReplace = Object.keys(templatePackage).filter(
    key =>
      !templatePackageBlacklist.includes(key) && !templatePackageToMerge.includes(key)
  )

  // Copy over some of the devDependencies
  appPackage.dependencies = appPackage.dependencies || {}

  // Setup the script rules
  const templateScripts = templatePackage.scripts || {}
  appPackage.scripts = {
    start: "react-scripts start",
    build: "react-scripts build",
    test: "react-scripts test",
    eject: "react-scripts eject",
    ...templateScripts,
  }

  // Update scripts for Yarn users
  if (useYarn) {
    appPackage.scripts = mapValues(appPackage.scripts, value =>
      value.replace(/(npm run |npm )/, "yarn ")
    )
  }

  // Setup the eslint config
  appPackage.eslintConfig = {
    extends: "react-app",
  }

  // Setup the browsers list
  appPackage.browserslist = defaultBrowsers

  // Add templatePackage keys/values to appPackage, replacing existing entries
  templatePackageToReplace.forEach(key => {
    appPackage[key] = templatePackage[key]
  })

  writeFileSync(join(appPath, "package.json"), JSON.stringify(appPackage, null, 2) + EOL)

  const readmeExists = existsSync(join(appPath, "README.md"))
  if (readmeExists) {
    renameSync(join(appPath, "README.md"), join(appPath, "README.old.md"))
  }

  // Copy the files for the user
  const templateDir = join(templatePath, "template")
  if (existsSync(templateDir)) {
    copySync(templateDir, appPath)
  } else {
    error(`Could not locate supplied template: ${green(templateDir)}`)
    return
  }

  // modifies README.md commands based on user used package manager.
  if (useYarn) {
    try {
      const readme = readFileSync(join(appPath, "README.md"), "utf8")
      writeFileSync(
        join(appPath, "README.md"),
        readme.replace(/(npm run |npm )/g, "yarn "),
        "utf8"
      )
    } catch {
      // Silencing the error. As it fall backs to using default npm commands.
    }
  }

  const gitignoreExists = existsSync(join(appPath, ".gitignore"))
  if (gitignoreExists) {
    // Append if there's already a `.gitignore` file there
    const data = readFileSync(join(appPath, "gitignore"))
    appendFileSync(join(appPath, ".gitignore"), data)
    unlinkSync(join(appPath, "gitignore"))
  } else {
    // Rename gitignore after the fact to prevent npm from renaming it to .npmignore
    // See: https://github.com/npm/npm/issues/1862
    moveSync(join(appPath, "gitignore"), join(appPath, ".gitignore"))
  }

  // Initialize git repo
  let initializedGit = false

  if (tryGitInit()) {
    initializedGit = true
    log()
    log("Initialized a git repository.")
  }

  let command: string
  let remove: string
  let args: string[]

  if (useYarn) {
    command = "yarnpkg"
    remove = "remove"
    args = ["add"]
  } else {
    command = "npm"
    remove = "uninstall"
    args = ["install", "--save", verbose && "--verbose"].filter(Boolean)
  }

  // Install additional template dependencies, if present.
  const dependenciesToInstall = Object.entries({
    ...templatePackage.dependencies,
    ...templatePackage.devDependencies,
  })
  if (dependenciesToInstall.length) {
    args.push(
      ...dependenciesToInstall.map(([dependency, version]) => `${dependency}@${version}`)
    )
  }

  // Install react and react-dom for backward compatibility with old CRA cli
  // which doesn't install react and react-dom along with react-scripts
  if (!isReactInstalled(appPackage)) {
    args.push("react", "react-dom")
  }

  // Install template dependencies, and react and react-dom if missing.
  if ((!isReactInstalled(appPackage) || templateName) && args.length > 1) {
    log()
    log(`Installing template dependencies using ${command}...`)

    const proc = sync(command, args, { stdio: "inherit" })
    if (proc.status !== 0) {
      error(`\`${command} ${args.join(" ")}\` failed`)
      return
    }
  }

  if (args.find(arg => arg.includes("typescript"))) {
    log()
    verifyTypeScriptSetup()
  }

  // Remove template
  log(`Removing template package using ${command}...`)
  log()

  const proc = sync(command, [remove, templateName], {
    stdio: "inherit",
  })
  if (proc.status !== 0) {
    error(`\`${command} ${args.join(" ")}\` failed`)
    return
  }

  // Create git commit if git repo was initialized
  if (initializedGit && tryGitCommit(appPath)) {
    log()
    log("Created git commit.")
  }

  // Display the most elegant way to cd.
  // This needs to handle an undefined originalDirectory for
  // backward compatibility with old global-cli's.
  let cdPath: string
  if (originalDirectory && join(originalDirectory, appName) === appPath) {
    cdPath = appName
  } else {
    cdPath = appPath
  }

  // Change displayed command to yarn instead of yarnpkg
  const displayedCommand = useYarn ? "yarn" : "npm"

  log(chalk`
  
    Success! Created ${appName} at ${appPath}
    Inside that directory, you can run several commands:

      {cyan ${displayedCommand} start}
      Starts the development server.

      {cyan ${displayedCommand} ${useYarn ? "" : "run "}build}
      Bundles the app into static files for production.

      {cyan ${displayedCommand} test}
      Starts the test runner.

      {cyan ${displayedCommand} ${useYarn ? "" : "run "}eject}
      Removes this tool and copies build dependencies, configuration files
      and scripts into the app directory. If you do this, you can’t go back!

    We suggest that you begin by typing:

      {cyan cd} ${cdPath}
      {cyan ${displayedCommand} start}
  `)
  if (readmeExists) {
    log()
    log(yellow("You had a `README.md` file, we renamed it to `README.old.md`"))
  }
  log()
  log("Happy hacking!")
}

function isReactInstalled(appPackage: { dependencies?: Record<string, string> }) {
  const dependencies = appPackage.dependencies || {}
  return dependencies.react != null && dependencies["react-dom"] != null
}

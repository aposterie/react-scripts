#!/usr/bin/env node
import { resolve } from "path"
import { realpathSync, readJSON, writeJSON } from "fs-extra"
import fetch from "node-fetch"

main().catch(err => {
  throw err
})

// Does not match the exact two package.json files -- this is an approximate type
type PackageJSON = typeof import("../../package.json")

async function gitlab(repoID: string, branchName: string) {
  const req = await fetch(
    `https://gitlab.com/api/v4/projects/${repoID}/repository/branches`
  )
  const branches: {
    name: string
    commit: {
      id: string
      short_id: string
      created_at: string
      parent_ids: string | null
      title: string
      message: string
      author_name: string
      author_email: string
      authored_date: string
      committer_name: string
      committer_email: string
      committed_date: string
      web_url: string
    }
    merged: boolean
    protected: boolean
    developers_can_push: boolean
    developers_can_merge: boolean
    can_push: boolean
    default: boolean
    web_url: string
  }[] = await req.json()

  const branch = branches.find(b => b.name.toLowerCase() === branchName.toLowerCase())
  return { id: branch.commit.short_id, date: branch.commit.committed_date }
}

async function main() {
  const { name, repository }: PackageJSON = require("../package.json")

  const appDirectory = realpathSync(process.cwd())
  const packageJSON = resolve(appDirectory, "package.json")

  const json: PackageJSON = await readJSON(packageJSON)
  const dependencyObject = json.dependencies?.[name]
    ? json.dependencies
    : json.devDependencies?.[name]
    ? json.devDependencies
    : undefined

  if (!dependencyObject) {
    throw Error(`${name} not found in dependency.`)
  }

  const currentVersion: string = dependencyObject[name]
  if (!/^gitlab:[\w]+\/[\w-]+#[\w\d]+$/.test(currentVersion)) {
    throw Error(`Invalid current version: ${currentVersion}`)
  }

  const [repoID, oldShortId] = currentVersion.split("#")

  const { id: newShortId, date: commitDate } = await gitlab(repository.id, "release")

  if (oldShortId === newShortId) {
    console.log("Already using the newest version")
    return
  }

  dependencyObject[name] = `${repoID}#${newShortId}`
  await writeJSON(packageJSON, json, { spaces: 2 })

  console.log(
    `Updated to commit ${newShortId} from ${oldShortId} on ${new Date(
      commitDate
    ).toLocaleString()}`
  )
}

/* eslint-disable @typescript-eslint/triple-slash-reference */
/// <reference types="webpack" />

interface HotModuleReplacementApplyOptions {
  ignoreUnaccepted?: boolean
  ignoreDeclined?: boolean
  ignoreErrored?: boolean
  onDeclined?(info: HotModuleReplacementInfo): void
  onUnaccepted?(info: HotModuleReplacementInfo): void
  onAccepted?(info: HotModuleReplacementInfo): void
  onDisposed?(info: HotModuleReplacementInfo): void
  onErrored?(info: HotModuleReplacementInfo): void
}

interface HotModuleReplacementInfo {
  type:
    | "self-declined"
    | "declined"
    | "unaccepted"
    | "accepted"
    | "disposed"
    | "accept-errored"
    | "self-accept-errored"
    | "self-accept-error-handler-errored"
  moduleId: number // The module in question.
  dependencyId: number // For errors: the module id owning the accept handler.
  chain: number[] // For declined/accepted/unaccepted: the chain from where the update was propagated.
  parentId: number // For declined: the module id of the declining parent
  outdatedModules: number[] // For accepted: the modules that are outdated and will be disposed
  outdatedDependencies: {
    // For accepted: The location of accept handlers that will handle the update
    [key: number]: number[]
  }
  error: Error // For errors: the thrown error
  originalError: Error // For self-accept-error-handler-errored:
  // the error thrown by the module before the error handler tried to handle it.
}

type HotModuleReplacementStatus =
  /** The process is waiting for a call to check (see below) */
  | "idle"
  /** The process is checking for updates */
  | "check"
  /** The process is getting ready for the update (e.g. downloading the updated module) */
  | "prepare"
  /** The update is prepared and available */
  | "ready"
  /** The process is calling the dispose handlers on the modules that will be replaced */
  | "dispose"
  /** The process is calling the accept handlers and re-executing self-accepted modules */
  | "apply"
  /** An update was aborted, but the system is still in its previous state */
  | "abort"
  /** An update has thrown an exception and the system’s state has been compromised */
  | "fail"

declare interface NodeModule {
  // https://webpack.js.org/api/hot-module-replacement/
  hot?: {
    accept(errorHandler: () => void): void
    accept(dependencies: string | string[], callback: () => void): void
    decline(): void
    decline(dependencies: string | string[]): void
    dispose(callback: (data: any) => void): void
    invalidate(): void
    status(): HotModuleReplacementStatus

    /**
     * Test all loaded modules for updates and, if updates exist, apply them.
     *
     * @example
     *
     * module.hot.check(autoApply).then(outdatedModules => {
     *   // outdated modules...
     * }).catch(error => {
     *   // catch errors
     * });
     *
     * @param autoApply can either be a boolean or options to pass to the apply method when called.
     */
    check(autoApply: boolean | HotModuleReplacementApplyOptions): Promise<any[]>
    apply(options: HotModuleReplacementApplyOptions): Promise<any>
    addStatusHandler(handler: (status: HotModuleReplacementStatus) => void): void
    removeStatusHandler(handler: (status: HotModuleReplacementStatus) => void): void
  }
}

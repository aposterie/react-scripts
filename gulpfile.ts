import { src, dest, parallel, TaskFunction } from "gulp"
import babel from "gulp-babel"
import ts from "gulp-typescript"
import prettier from "gulp-prettier"
import edit from "./tasks/gulp-json-editor"

const last = <T>(value: ArrayLike<T>): T => value[value.length - 1]

const DIST = "lib"

const parallelObj = (value: Record<string, TaskFunction>) =>
  parallel(
    ...Object.entries(value).map(([name, fn]) => {
      Object.defineProperty(fn, "name", { value: name })
      exports[name] = fn
      return fn
    })
  )

const build = (name: string, dist = DIST) =>
  src([`${name.slice(2)}/**/*.ts`])
    .pipe(ts({ target: "ES2020", isolatedModules: true }))
    .pipe(babel())
    .pipe(prettier())
    .pipe(dest(`${dist}/${last(name.split("/"))}`))

export default parallelObj({
  bin: () => build("./src/bin"),
  config: () => build("./src/config"),
  scripts: () => build("./src/scripts"),
  devUtils: () => build("./src/react-dev-utils"),
  babelPreset: () => build("./src/babel-preset-react-app"),
  copyFile: parallelObj({
    types: () => src("./src/*.d.ts").pipe(dest(DIST)),
    package: () =>
      src("package.json")
        .pipe(
          edit(pkg => {
            delete pkg.devDependencies
            delete pkg.husky
            delete pkg.scripts
            delete pkg["lint-staged"]
            delete pkg["prettier"]
          })
        )
        .pipe(dest(DIST)),
  }),
})

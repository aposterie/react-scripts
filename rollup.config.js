// @ts-check
/// <reference path="./src/react-app.d.ts" />
import babel from "@rollup/plugin-babel"
import prettier from "rollup-plugin-prettier"
import node from "@rollup/plugin-node-resolve"
import shebang from "rollup-plugin-preserve-shebang"
import scss from "./tasks/rollup-scss-emotion"
import commonjs from "@rollup/plugin-commonjs"
import replace from "@rollup/plugin-replace"
import { terser } from "rollup-plugin-terser"

const extensions = [".js", ".ts", ".tsx"]

process.env.ROLLUP = "true"

/**
 * @param {string} input
 * @param {string} output
 * @param {boolean} node_modules
 * @returns {import("rollup").RollupOptions}
 */
const build = (
  input,
  output = input.replace(/^(\.\/)?src\//, "").replace(/\.\w+$/, ".js"),
  node_modules = false
) => ({
  input,
  output: {
    file: "lib/" + output,
    format: "cjs",
    banner: "/* eslint-disable */",
  },
  external: node_modules ? () => false : module => !module.startsWith("."),
  plugins: [
    shebang(),
    scss(),
    replace({
      "process.env.NODE_ENV": "'production'",
      "process.env": `({ NODE_ENV: "production" })`,
      "process.platform": "undefined",
      __REACT_DEVTOOLS_GLOBAL_HOOK__: `({})`,
    }),
    {
      load: id => (/^os\??/.test(id) ? "export const EOL = '\n'" : null),
    },
    babel({
      extensions,
      babelHelpers: "inline",
      comments: false,
    }),
    node({
      extensions,
      mainFields: ["browser", "jsnext:main", "module", "main"],
    }),
    prettier({
      parser: "babel",
    }),
    node_modules ? commonjs() : {},
    node_modules ? terser() : {},
  ],
})

export default [
  build("./src/bin/react-scripts.ts"),
  build("./src/react-error-overlay/index.ts"),
  build("./src/react-error-overlay/iframeScript.tsx", undefined, true),
]

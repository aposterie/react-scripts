const { useDedentPlugin } = require("babel-dedentor")

// This configuration is only used to build react-scripts files.
module.exports = {
  plugins: [
    ["@babel/plugin-transform-typescript", { allowNamespace: true, isTSX: true }],
    "@babel/plugin-transform-react-jsx",
    "babel-plugin-macros",
    ["@emotion/babel-plugin", { sourceMap: false }],
    process.env.ROLLUP ? false : "@babel/plugin-transform-modules-commonjs",
    process.env.ROLLUP ? false : "@babel/plugin-transform-runtime",
    ["@babel/plugin-proposal-class-properties", { loose: true }],
    ["@babel/plugin-proposal-optional-chaining", { loose: true }],
    "@babel/plugin-proposal-optional-catch-binding",
    ["@babel/plugin-proposal-nullish-coalescing-operator", { loose: true }],
    "babel-plugin-minify-constant-folding",
    "babel-plugin-minify-dead-code-elimination",
    "babel-plugin-minify-guarded-expressions",
    useDedentPlugin({
      keepFunctionCall: true,
      shouldDedent: (callee, t, matchIdentifier) =>
        matchIdentifier(callee, "chalk") ||
        (t.isMemberExpression(callee) && matchIdentifier(callee.object, "chalk")),
    }),
  ].filter(Boolean),
}

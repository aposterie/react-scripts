# react-scripts

<img alt="Logo" align="right" src="https://create-react-app.dev/img/logo.svg" width="20%" />

Fully typed, modern `react-scripts` fork.

[![Upstream status](https://img.shields.io/badge/upstream-%239722ef1-blue.svg)](https://github.com/facebook/create-react-app/compare/9722ef1...master?w=1)

## Additional [advanced configuration](https://create-react-app.dev/docs/advanced-configuration/)

| env variable   | Description                           |
| -------------- | ------------------------------------- |
| `KEEP_CONSOLE` | Do not clear console upon new message |

## Quick Overview

```sh
yarn remove react-scripts
yarn add gitlab:aposterie/react-scripts#f94accd
```

Then open [http://localhost:3000/](http://localhost:3000/) to see your app.<br>
When you’re ready to deploy to production, create a minified bundle with `npm run build`.

To update this script, use `npx update-react-scripts`.

## What’s included?

In additional to what `react-scripts` offers, this fork also has:

- [worker-plugin](https://github.com/GoogleChromeLabs/worker-plugin) support
- Webpack [module alias](https://webpack.js.org/configuration/resolve/#resolvealias) support
- [`react-app-rewired`](https://github.com/timarney/react-app-rewired) and [`customize-cra`](https://github.com/arackaf/customize-cra) support out-of-the-box. Just put `config-overrides.{js,ts}` in the root directory and you’re good to go.
- No Internet Explorer support.

### `react-scripts`

Your environment will have everything you need to build a modern single-page React app:

- React, JSX, ES6, TypeScript ~~and Flow~~ syntax support.
- Language extras beyond ES6 like the object spread operator.
- Autoprefixed CSS, so you don’t need `-webkit-` or other prefixes.
- A fast interactive unit test runner with built-in support for coverage reporting.
- A live development server that warns about common mistakes.
- A build script to bundle JS, CSS, and images for production, with hashes and sourcemaps.
- An offline-first [service worker](https://developers.google.com/web/fundamentals/getting-started/primers/service-workers) and a [web app manifest](https://developers.google.com/web/fundamentals/engage-and-retain/web-app-manifest/), meeting all the [Progressive Web App](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app) criteria. (_Note: Using the service worker is opt-in as of `react-scripts@2.0.0` and higher_)
- Hassle-free updates for the above tools with a single dependency.

Check out [this guide](https://github.com/nitishdayal/cra_closer_look) for an overview of how these tools fit together.

## Credits

This project exists thanks to all the people who [contribute](https://github.com/facebook/create-react-app/blob/master/CONTRIBUTING.md).

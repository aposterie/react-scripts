declare module "detect-port-alt" {
  function detect(port: number): Promise<number>
  export = detect
}

declare module "bfj" {
  export function write(path: string, data: any, options?: object): Promise<void>
}
declare module "postcss-normalize"
declare module "pnp-webpack-plugin"
declare module "escape-string-regexp"
declare module "find-up"

declare module "babel-loader"
declare module "file-loader"
declare module "url-loader"
declare module "css-loader"
declare module "postcss-loader"
declare module "eslint-loader"

declare module "postcss-preset-env"

declare module "@babel/preset-react"
declare module "@babel/plugin-transform-typescript"
declare module "@babel/plugin-proposal-decorators"
declare module "@babel/plugin-proposal-class-properties"
declare module "@babel/plugin-proposal-numeric-separator"
declare module "@babel/plugin-proposal-optional-chaining"
declare module "@babel/plugin-proposal-nullish-coalescing-operator"
declare module "@babel/plugin-proposal-logical-assignment-operators"
declare module "babel-plugin-macros"
declare module "react-error-overlay"

declare module "@babel/code-frame" {
  export function codeFrameColumns(
    rawLines: string,
    location: { start: { line: number; column: number } }
  ): string
}
